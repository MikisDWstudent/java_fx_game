package Parchis;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import Parchis.model.ParchisModel;
import Parchis.view.start.StartPresenter;
import Parchis.view.start.StartView;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        ParchisModel model = new ParchisModel();
        StartView view = new StartView();
        StartPresenter startPresenter = new StartPresenter(model,view);
        Scene scene = new Scene(view);
        scene.getStylesheets().add("/stylesheets/style.css");
        primaryStage.setScene(scene);
        //open linksboven in het scherm
        primaryStage.setX(100);
        primaryStage.setY(100);
        //Belangrijk dat dit na setScene komt, anders werkt dit niet
        startPresenter.addWindowEventHandlers();
        primaryStage.setTitle("Parchis spel");
        primaryStage.show();
    }
    public static void main(String[] args) {
        Application.launch(args);
    }
}
