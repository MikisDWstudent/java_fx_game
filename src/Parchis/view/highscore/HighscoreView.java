package Parchis.view.highscore;

import Parchis.view.game.GridBoard;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

public class HighscoreView extends BorderPane {
    private TextArea taHighscore = new TextArea();
    private Button btnSluit;
    private Button btnSwapSort;
    private Region spacingBtwnBtns;
    private HBox navMenu;

    public HighscoreView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        btnSluit = new Button("Sluiten");
        btnSwapSort = new Button("draai volgorde om");
        //we want btnSluit at the right and btwnSwapSort at the left, so apply a trick:
        //https://stackoverflow.com/questions/41654333/how-to-align-children-in-a-hbox-left-center-and-right/41654413
        spacingBtwnBtns = new Region();
    }

    private void layoutNodes() {

        Label info = new Label("Onze top performers: ");
        setTop(info);

        setCenter(taHighscore);

        navMenu = new HBox();
        HBox.setHgrow(spacingBtwnBtns, Priority.ALWAYS);
        navMenu.setPadding(new Insets(15, 12, 15, 12));
        navMenu.setSpacing(10);
        navMenu.getChildren().addAll(btnSwapSort, spacingBtwnBtns, btnSluit);
        setBottom(navMenu);


        taHighscore.setPrefWidth(Double.MAX_VALUE);
        taHighscore.setPrefHeight(Double.MAX_VALUE);
        taHighscore.setWrapText(true);
        taHighscore.setEditable(false);
        setPrefWidth(600);
        setPrefHeight(500);
        setPadding(new Insets(10));
        BorderPane.setMargin(info, new Insets(10, 0, 10, 0));
        BorderPane.setMargin(btnSluit, new Insets(10, 0, 0, 0));
        BorderPane.setMargin(btnSwapSort, new Insets(10, 0, 0, 0));
    }

    public TextArea getTaHighscore() {
        return taHighscore;
    }

    public Button getBtnSluit() {
        return btnSluit;
    }

    public Button getBtnSwapSort() {
        return btnSwapSort;
    }
}
