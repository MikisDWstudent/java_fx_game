package Parchis.view.highscore;

import Parchis.model.Highscore;
import Parchis.model.ParchisException;
import Parchis.model.ParchisModel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class HighscorePresenter {

    private HighscoreView view;
    private final ParchisModel model;

    public HighscorePresenter(ParchisModel model, HighscoreView view) {
        this.model = model;
        try {
            String score = model.getHighscore().toString();
            view.getTaHighscore().setText(score);
        } catch (ParchisException me) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(me.getMessage());
            alert.showAndWait();
        }

        view.getBtnSluit().setOnAction(new EventHandler<ActionEvent>() {
            //sluit de pagina
            @Override
            public void handle(ActionEvent actionEvent) {
                Node source = (Node) actionEvent.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                stage.close();
            }
        });
        view.getBtnSwapSort().setOnAction(new EventHandler<ActionEvent>() {
            //draai de orde om waarin de highscores getoond worden
            @Override
            public void handle(ActionEvent actionEvent) {
                model.getHighscore().swapSortOrder();
                String score = model.getHighscore().toString();
                view.getTaHighscore().setText(score);
            }
        });
    }
}
