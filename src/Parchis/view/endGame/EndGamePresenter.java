package Parchis.view.endGame;

import Parchis.model.ParchisModel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.stage.Stage;

public class EndGamePresenter {

    private final EndGameView view;
    private final ParchisModel model;

    public EndGamePresenter(ParchisModel model, EndGameView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
    }

    private void addEventHandlers() {
        view.getBtnVoegScoreToe().setOnAction(new EventHandler<ActionEvent>() {
            //sluit scherm
            @Override
            public void handle(ActionEvent actionEvent) {
                //voeg record toe
                int aantalBeurten = view.getBeurtScore();
                String spelerNaam = view.getTfSpelerNaam().getText();

                //voeg record toe & schrijf weg naar de txt file
                model.getHighscore().addHighscoreRecord(spelerNaam, aantalBeurten);
                model.getHighscore().sortHighscoreAscending(true);
                model.getHighscore().writeFile();

                //sluit deze stage
                Node source = (Node) actionEvent.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                stage.close();

            }
        });
    }
}