package Parchis.view.endGame;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;

public class EndGameView extends BorderPane {
    private Button BtnVoegScoreToe;
    private Label lblSpelerNaam;
    private TextField tfSpelerNaam;
    private final int beurtScore;
    private final String spelerNaam;
    private Label lblSpelerBeurtAantal;
    private Label lblSpelerBeurtAantalInfo;

    public EndGameView(int beurtScore, String spelerNaam) {
        this.beurtScore = beurtScore;
        this.spelerNaam = spelerNaam;
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        lblSpelerNaam = new Label("geef hieronder je naam in");
        tfSpelerNaam = new TextField(spelerNaam);
        BtnVoegScoreToe = new Button("voeg toe aan highscore");
        lblSpelerBeurtAantal  = new Label("U had zoveel beurten nodig om te winnen:");
        lblSpelerBeurtAantalInfo = new Label(String.valueOf(this.beurtScore));
    }

    private void layoutNodes() {
        GridPane scorePane = new GridPane();
        scorePane.add(lblSpelerNaam, 0, 0);
        scorePane.add(tfSpelerNaam, 0, 1);
        scorePane.add(lblSpelerBeurtAantal, 1, 0);
        scorePane.add(lblSpelerBeurtAantalInfo, 1, 1);

        Insets insets = new Insets(15, 12, 15, 12);
        GridPane.setMargin(lblSpelerNaam, insets);
        GridPane.setMargin(tfSpelerNaam, insets);
        GridPane.setMargin(lblSpelerBeurtAantal, insets);
        GridPane.setMargin(lblSpelerBeurtAantalInfo, insets);
        GridPane.setHalignment(lblSpelerBeurtAantalInfo, HPos.CENTER);

        setTop(new Label("Proficiat! U hebt gewonnen!"));
        setCenter(scorePane);
        setBottom(BtnVoegScoreToe);

        setPrefWidth(600);
        setPrefHeight(200);
        setPadding(new Insets(10));
    }

    public Button getBtnVoegScoreToe() {
        return BtnVoegScoreToe;
    }

    public TextField getTfSpelerNaam() {
        return tfSpelerNaam;
    }

    public int getBeurtScore() {
        return beurtScore;
    }
}
