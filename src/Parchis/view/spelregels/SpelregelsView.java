package Parchis.view.spelregels;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;

/**
 * @author Inez Cuypers
 * @version 1.0 19/02/2021 10:52
 */
public class SpelregelsView extends BorderPane {
    private TextArea taSpelregels = new TextArea();
    private Button btnOke;

    public SpelregelsView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        btnOke = new Button("Sluiten");
        btnOke.setPrefWidth(60);
    }

    private void layoutNodes() {
        Label info = new Label("Wat u zoal moet weten: ");
        setTop(info);
        setCenter(taSpelregels);
        taSpelregels.setPrefWidth(Double.MAX_VALUE);
        taSpelregels.setPrefHeight(Double.MAX_VALUE);
        taSpelregels.setWrapText(true);
        taSpelregels.setEditable(false);
        setPrefWidth(600);
        setPrefHeight(500);
        setPadding(new Insets(20));
        BorderPane.setMargin(info, new Insets(10, 0, 10, 0));
        BorderPane.setAlignment(btnOke, Pos.CENTER_RIGHT);
        BorderPane.setMargin(btnOke, new Insets(10, 0, 0, 0));
        setBottom(btnOke);
    }

    TextArea getTaSpelregels() { return taSpelregels; }
    Button getBtnOke() {
        return btnOke;
    }
}
