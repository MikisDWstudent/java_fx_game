package Parchis.view.start;

import Parchis.model.ParchisModel;
import Parchis.view.game.ParchisPresenter;
import Parchis.view.game.ParchisView;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.WindowEvent;

import java.nio.file.Path;

public class StartPresenter {
    /*
   In MVP, the presenter assumes the functionality of the "middle-man".
   In MVP, all presentation logic is pushed to the presenter
   The presenter acts upon the model and the view. It retrieves data from
   repositories (the model), and formats it for display in the view.
   */
    private final ParchisModel model;
    private final StartView view;

    public StartPresenter(ParchisModel model, StartView view) {
        this.model = model;
        this.view = view;
        addEventHandlers();
        updateView();
    }

    private void updateView() {
        //fill view with data from model
    }

    private void addEventHandlers() {
        //forward events to calls in model

        view.getBtnStartSpel().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ParchisView parchisView = new ParchisView(view);
                ParchisModel parchisModel = new ParchisModel();
                ParchisPresenter parchisPresenter = new ParchisPresenter(parchisModel, parchisView);
                view.getScene().setRoot(parchisView);
                parchisView.getScene().getWindow().sizeToScene();
            }
        });
        view.getCheatModus().setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        if (view.getCheatModus().isSelected()) {
                            view.getCheatModus().setSelected(true);
                            view.getCheatModus().setText("Cheat modus aan");
                        } else {
                            view.getCheatModus().setSelected(false);
                            view.getCheatModus().setText("Cheat modus uit");
                        }
                    }
                });
    }
    public void addWindowEventHandlers() {
        view.getScene().getWindow().setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setHeaderText("Het spel afsluiten");
                alert.setContentText("U staat op het punt het spel af te sluiten" + "\n"
                + "Hierdoor gaat uw voortgang verloren. Bent u zeker?");
                alert.setTitle("Opgelet");
                alert.getButtonTypes().clear();
                ButtonType neen = new ButtonType("Nee, ga terug");
                ButtonType ja = new ButtonType("Ja, sluit spel af");
                alert.getButtonTypes().addAll(neen, ja);
                alert.showAndWait();
                if (alert.getResult() == null || alert.getResult().equals(neen)) {
                    event.consume();
                }
            }
        });
    }
}
