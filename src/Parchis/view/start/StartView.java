package Parchis.view.start;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class StartView extends GridPane {
    /*
    The view is a passive interface that displays data (the model)
    and routes user commands (events) to the presenter to act upon
    that data.
     */
    private Button btnStartSpel;
    private Label lblWelcome;
    private Label lblUitlegCheat;
    private ImageView PawnBlue;
    private ImageView PawnYellow;
    private ImageView PawnRed;
    private ImageView PawnGreen;
    private ToggleButton cheatModus;
    private Label lblRodeSpeler;
    private Label lblGroeneSpeler;
    private Label lblGeleSpeler;
    private Label lblBlauweSpeler;
    private TextField tfRodeSpeler;
    private TextField tfGroeneSpeler;
    private TextField tfGeleSpeler;
    private TextField tfBlauweSpeler;

    public StartView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        Image imgCheat = new Image("/icons/cheat_white.png");
        cheatModus = new ToggleButton("Cheat modus uit", new ImageView(imgCheat));
        cheatModus.setSelected(false);
        btnStartSpel = new Button("Start nieuw spel");
        //taWelcome = new TextArea("Welcome in onze versie van Parchis");
        lblWelcome = new Label("Welcome in onze versie van Parchis. \nOm een nieuw spel te starten, klikt u op Start nieuw spel");

        lblRodeSpeler = new Label("Naam voor de rode speler");
        lblGroeneSpeler = new Label("Naam voor de groene speler");
        lblGeleSpeler = new Label("Naam voor de gele speler");
        lblBlauweSpeler = new Label("Naam voor de blauwe speler");

        tfRodeSpeler = new TextField("Rode Ridder");
        tfGroeneSpeler = new TextField("Donatello");
        tfGeleSpeler = new TextField("Tweety");
        tfBlauweSpeler = new TextField("Grote smurf");

        lblUitlegCheat = new Label("Zet cheat modus aan om het spel versneld te spelen");
        PawnBlue = new ImageView( new Image("/icons/chess-pawn-blue.png"));
        PawnYellow = new ImageView( new Image("/icons/chess-pawn-yellow.png"));
        PawnRed = new ImageView( new Image("/icons/chess-pawn-red.png"));
        PawnGreen = new ImageView( new Image("/icons/chess-pawn-green.png"));
    }

    private void layoutNodes() {
        cheatModus.setMinWidth(140); // Anders groeit hij als je hem aan zet
        lblUitlegCheat.setWrapText(true); // Als het scherm te smal is voor de tekst, komt hij op de volgende rij
        lblWelcome.setWrapText(true);
        this.add(PawnBlue, 0, 0);
        this.add(PawnGreen, 1, 0);
        this.add(lblWelcome, 2, 0);
        this.add(PawnRed, 3, 0);
        this.add(PawnYellow, 4, 0);

        this.add(lblRodeSpeler, 0, 1, 2, 1);
        this.add(lblBlauweSpeler, 0, 2, 2, 1);
        this.add(lblGeleSpeler, 0, 3, 2, 1);
        this.add(lblGroeneSpeler, 0, 4, 2, 1);

        this.add(tfRodeSpeler, 2, 1);
        this.add(tfBlauweSpeler, 2, 2);
        this.add(tfGeleSpeler, 2, 3);
        this.add(tfGroeneSpeler, 2, 4);

        this.add(lblUitlegCheat, 0, 5, 2, 1);
        this.add(cheatModus, 0, 6, 2, 1);
        this.add(btnStartSpel, 3, 6, 2, 1);

        // Door setVgrow op één item van een rij te zetten, laten we de hele rij groeien
        GridPane.setVgrow(PawnBlue, Priority.ALWAYS);

        //Definieer de breedte van de kolommen
        ColumnConstraints col0 = new ColumnConstraints();
        col0.setPercentWidth(15.0);
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(15.0);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(40.0);
        ColumnConstraints col3 = new ColumnConstraints();
        col3.setPercentWidth(15.0);
        ColumnConstraints col4 = new ColumnConstraints();
        col4.setPercentWidth(15.0);
        this.getColumnConstraints().addAll(col0, col1, col2, col3, col4);

        // Centreer alles
        GridPane.setHalignment(btnStartSpel, HPos.CENTER);
        GridPane.setHalignment(PawnBlue, HPos.CENTER);
        GridPane.setHalignment(PawnGreen, HPos.CENTER);
        GridPane.setHalignment(PawnRed, HPos.CENTER);
        GridPane.setHalignment(PawnYellow, HPos.CENTER);
        GridPane.setHalignment(lblWelcome, HPos.CENTER);

        //zorg ervoor dat de knoppen niet aan de rand kleven
        GridPane.setMargin(lblRodeSpeler, new Insets(5, 5, 5, 5));
        GridPane.setMargin(lblBlauweSpeler, new Insets(5, 5, 5, 5));
        GridPane.setMargin(lblGeleSpeler, new Insets(5, 5, 5, 5));
        GridPane.setMargin(lblGroeneSpeler, new Insets(5, 5, 5, 5));

        GridPane.setMargin(tfRodeSpeler, new Insets(5, 5, 5, 5));
        GridPane.setMargin(tfBlauweSpeler, new Insets(5, 5, 5, 5));
        GridPane.setMargin(tfGeleSpeler, new Insets(5, 5, 5, 5));
        GridPane.setMargin(tfGroeneSpeler, new Insets(5, 5, 5, 5));

        GridPane.setMargin(lblUitlegCheat, new Insets(10, 5, 0, 5));
        GridPane.setMargin(cheatModus, new Insets(5, 5, 5, 5));
        GridPane.setMargin(btnStartSpel, new Insets(5, 5, 5, 5));

        setPrefSize(600,300);
        //this.setGridLinesVisible(true);
    }
    // door geen public getters te maken, zorg je er voor dat enkel de Presenter er aan kan
    Button getBtnStartSpel() {
        return btnStartSpel;
    }

    Label getLblWelcome() {
        return lblWelcome;
    }

    public TextField getTfRodeSpeler() {
        return tfRodeSpeler;
    }

    public TextField getTfGroeneSpeler() {
        return tfGroeneSpeler;
    }

    public TextField getTfGeleSpeler() {
        return tfGeleSpeler;
    }

    public TextField getTfBlauweSpeler() {
        return tfBlauweSpeler;
    }

    public ToggleButton getCheatModus() {
        return cheatModus;
    }
}
