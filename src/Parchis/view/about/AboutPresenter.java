package Parchis.view.about;

import Parchis.model.LeesFile;
import Parchis.model.ParchisException;
import Parchis.model.TxtReader;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class AboutPresenter {

    public AboutPresenter(AboutView view) {

        Path filePath = Paths.get("resources" + File.separator+ "bestanden" + File.separator + "about.txt");
        TxtReader txtReader = new TxtReader(filePath);

        try {
            view.getTaAbout().setText(txtReader.getContent());
        } catch (ParchisException me) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText(me.getMessage());
            alert.showAndWait();
        }

        view.getBtnOke().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Node source = (Node) actionEvent.getSource();
                Stage stage = (Stage) source.getScene().getWindow();
                stage.close();
            }
        });
    }
}