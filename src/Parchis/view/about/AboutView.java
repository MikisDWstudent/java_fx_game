package Parchis.view.about;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.image.Image;

public class AboutView extends BorderPane {

    private final TextArea taAbout = new TextArea();
    private Button btnOke;
    private Image imgAuteur;
    private ImageView imgViewAuteur;

    public AboutView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        imgAuteur = new Image("/bestanden/auteur.jpg", 350, 140, true, false);
        imgViewAuteur = new ImageView(imgAuteur);
        btnOke = new Button("Sluiten");
        btnOke.setPrefWidth(60);
    }

    private void layoutNodes() {
        setTop(imgViewAuteur);
        setCenter(taAbout);
        taAbout.setPrefWidth(Double.MAX_VALUE);
        taAbout.setPrefHeight(Double.MAX_VALUE);
        taAbout.setWrapText(true);
        taAbout.setEditable(false);
        setPrefWidth(600);
        setPrefHeight(500);
        setPadding(new Insets(10));
        BorderPane.setAlignment(imgViewAuteur, Pos.CENTER);
        BorderPane.setAlignment(btnOke, Pos.CENTER_RIGHT);
        BorderPane.setMargin(taAbout, new Insets(25, 0, 0, 0));
        BorderPane.setMargin(btnOke, new Insets(10, 0, 0, 0));
        setBottom(btnOke);
    }

    TextArea getTaAbout() { return taAbout; }
    Button getBtnOke() {
        return btnOke;
    }
}
