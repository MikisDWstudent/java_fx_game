package Parchis.view.game;

import Parchis.model.Kleur;
import Parchis.model.ParchisException;
import Parchis.model.ParchisModel;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import Parchis.view.start.StartView;

public class GridInfo {
    private final GridPane info;
    private final Kleur startSpeler;
    private Button btnDobbel;
    private Label lblWorpInfo;
    private Label lblBeurt;
    private Label lblBeurtInfo;
    private Label lblKeerZesGegooid;
    private Label lblKeerZesGegooidInfo;
    private Label lblBeurtNummer;
    private Label lblBeurtNummerInfo;
    private Label lblWarning;
    private Label lblWarningInfo;
    private Label lblInfoPion1;
    private Label lblInfoPion2;
    private Label lblInfoPion3;
    private Label lblInfoPion4;
    private Label lblZetInfoPion1;
    private Label lblZetInfoPion2;
    private Label lblZetInfoPion3;
    private Label lblZetInfoPion4;

    private Button btnPion1;
    private Button btnPion2;
    private Button btnPion3;
    private Button btnPion4;

    private Label lblValsSpeelLocatie;
    private Label lblValsSpeelLocatieInfo;
    private TextField tfValsSpeelLocatie;
    private Label lblValsSpeelDobbel;
    private TextField tfValsSpeelDobbel;

    private Button btnValsSpeelPion1;
    private Button btnValsSpeelPion2;
    private Button btnValsSpeelPion3;
    private Button btnValsSpeelPion4;
    private Button btnValsSpeelDobbel;

    private Label lblValsSpeelInstructies;

    public GridInfo(Kleur startSpeler, StartView startView) { //startview om een referentie te maken aan cheatmodus
        //we hebben startspeler nodig als attribute omdat we een label hebben die dit weergeeft.
        //op het moment van initialisatie moeten we dus weten wie dat is
        this.startSpeler = startSpeler;
        info = new GridPane();
        initialiseNodes();
        layoutNodes();

        boolean cheatModusOn = startView.getCheatModus().isSelected();
        toggleCheatModus(cheatModusOn);
    }

    private void initialiseNodes() {
        btnDobbel = new Button("Gooi dobbelsteen");
        lblWorpInfo = new Label("U hebt nog niet geworpen");
        lblKeerZesGegooid = new Label("# keer zes gegooid");
        lblKeerZesGegooidInfo = new Label("0x");
        lblBeurt = new Label("De beurt is aan");
        lblBeurtInfo = new Label(startSpeler.toString());
        lblWarning = new Label("Feedback");
        lblBeurtNummer = new Label("Dit is beurt nummer");
        lblBeurtNummerInfo = new Label("1");
        lblWarningInfo = new Label(String.format("Er is random gedobbeld om te starten: speler %s begint", startSpeler.toString()));

        lblInfoPion1 = new Label("Huidige locatie: Nest");
        lblInfoPion2 = new Label("Huidige locatie: Nest");
        lblInfoPion3 = new Label("Huidige locatie: Nest");
        lblInfoPion4 = new Label("Huidige locatie: Nest");

        lblZetInfoPion1 = new Label("dobbel voor omschrijving");
        lblZetInfoPion2 = new Label("dobbel voor omschrijving");
        lblZetInfoPion3 = new Label("dobbel voor omschrijving");
        lblZetInfoPion4 = new Label("dobbel voor omschrijving");

        btnPion1 = new Button("doe actie op pion1");
        btnPion2 = new Button("doe actie op pion2");
        btnPion3 = new Button("doe actie op pion3");
        btnPion4 = new Button("doe actie op pion4");
        btnPion1.setDisable(true);
        btnPion2.setDisable(true);
        btnPion3.setDisable(true);
        btnPion4.setDisable(true);

        //enkel als vals speel modus = true
        lblValsSpeelLocatie = new Label("Verzet pion (speel vals)");
        lblValsSpeelLocatieInfo = new Label("gebruik 'speel vals pionX'");
        tfValsSpeelLocatie = new TextField("25");

        lblValsSpeelDobbel = new Label("werp dit getal (speel vals)");
        tfValsSpeelDobbel = new TextField("5");

        btnValsSpeelPion1 = new Button("speel vals pion1");
        btnValsSpeelPion2 = new Button("speel vals pion2");
        btnValsSpeelPion3 = new Button("speel vals pion3");
        btnValsSpeelPion4 = new Button("speel vals pion4");
        btnValsSpeelDobbel = new Button("overschrijf worp");

        lblValsSpeelInstructies = new Label("Vals speel instructies:\n" +
                "Omdat cheat modus aan staat, kan je nu de dobbelsteen overschrijven en eender welke pion verzetten\n" +
                "De dobbelsteen overschrijven doe je door een getal in te geven bij 'werp dit getal (speel vals)' en op 'overschrijf worp' te duwen.\n" +
                "Om een pion te verzetten geef je een getal in bij 'Verzet pion (speel vals)' en druk je op de knop naast de pion 'speel vals pion x'\n" +
                "Enkele speciale gevallen: de eerste gele landingzone is nummer 69, voor blauw is dat 86, voor rood 103 en voor groen 120.\n" +
                "Dat komt omdat blauw begint op plaats 22 (17 hoger dan geel, die op 5 begint) dus 69+17=86.\n" +
                "Een pion uitspelen doe je door locatie 999 in te geven. Let wel: als je een pion uitspeelt is dat zogezegd een zet.\n" +
                "Dat betekent ook dat als je andere pionnen in het nest zitten, dat je beurt dan voorbij is");
    }

    private void layoutNodes() {
        info.getStyleClass().add("infoGrid");
        info.add(btnDobbel, 0, 0);
        info.add(lblWorpInfo, 1, 0);
        info.add(lblBeurt, 0, 1);
        info.add(lblBeurtInfo, 1, 1);
        info.add(lblKeerZesGegooid, 0, 2);
        info.add(lblKeerZesGegooidInfo, 1, 2);
        info.add(lblBeurtNummer, 0, 3);
        info.add(lblBeurtNummerInfo, 1, 3);
        info.add(lblInfoPion1, 1, 4);
        info.add(lblInfoPion2, 1, 5);
        info.add(lblInfoPion3, 1, 6);
        info.add(lblInfoPion4, 1, 7);
        info.add(lblZetInfoPion1, 2, 4);
        info.add(lblZetInfoPion2, 2, 5);
        info.add(lblZetInfoPion3, 2, 6);
        info.add(lblZetInfoPion4, 2, 7);

        info.add(btnPion1, 0, 4);
        info.add(btnPion2, 0, 5);
        info.add(btnPion3, 0, 6);
        info.add(btnPion4, 0, 7);
        info.add(lblWarning, 0, 9, 1, 3);
        info.add(lblWarningInfo, 1, 9);
        lblWarningInfo.setWrapText(true); //aangezien we max width zetten, moet dit wrappen

        //enkel als vals speel modus = true
        info.add(lblValsSpeelLocatie, 0, 11);
        info.add(tfValsSpeelLocatie, 1, 11);
        info.add(lblValsSpeelLocatieInfo, 2,11);
        info.add(lblValsSpeelDobbel, 0, 10);
        info.add(tfValsSpeelDobbel, 1, 10);
        info.add(btnValsSpeelPion1, 3, 4);
        info.add(btnValsSpeelPion2, 3, 5);
        info.add(btnValsSpeelPion3, 3, 6);
        info.add(btnValsSpeelPion4, 3, 7);
        info.add(btnValsSpeelDobbel, 2, 10);
        info.add(lblValsSpeelInstructies, 0, 12, 4, 1);
        lblValsSpeelInstructies.setWrapText(true);

        //Zorg ervoor dat er niks tegen de rand kleeft
        Insets insets = new Insets(10, 10, 10, 30);
        setInsetsToChildren(info, insets);

        //Left alignment voor alle labels
        leftAlignChildrenLabels(info);
        GridPane.setValignment(lblWarning, VPos.TOP);

        ColumnConstraints column1 = new ColumnConstraints(175);
        ColumnConstraints column2 = new ColumnConstraints(200);
        info.getColumnConstraints().addAll(column1, column2);
    }

    private void toggleCheatModus(boolean cheatModusEnabled) {
        lblValsSpeelLocatie.setVisible(cheatModusEnabled);
        lblValsSpeelLocatieInfo.setVisible(cheatModusEnabled);
        tfValsSpeelLocatie.setVisible(cheatModusEnabled);
        lblValsSpeelDobbel.setVisible(cheatModusEnabled);
        tfValsSpeelDobbel.setVisible(cheatModusEnabled);
        btnValsSpeelPion1.setVisible(cheatModusEnabled);
        btnValsSpeelPion2.setVisible(cheatModusEnabled);
        btnValsSpeelPion3.setVisible(cheatModusEnabled);
        btnValsSpeelPion4.setVisible(cheatModusEnabled);
        btnValsSpeelDobbel.setVisible(cheatModusEnabled);
        lblValsSpeelInstructies.setVisible(cheatModusEnabled);
    }

    private void setInsetsToChildren(GridPane gridpane, Insets insets) {
        //https://www.codota.com/code/java/methods/javafx.scene.layout.GridPane/getChildren
        ObservableList<Node> childrens = gridpane.getChildren();
        for (Node node : childrens) {
            GridPane.setMargin(node, insets);
        }
    }

    private void leftAlignChildrenLabels(GridPane gridpane) {
        ObservableList<Node> childrens = gridpane.getChildren();
        for (Node node : childrens) {
            if(node instanceof Label) {
                GridPane.setHalignment(node, HPos.LEFT);
            }
        }
    }

    public GridPane getInfo() {
        return info;
    }

    public Button getBtnDobbel() {
        return btnDobbel;
    }

    public Label getLblWorpInfo() {
        return lblWorpInfo;
    }

    public Label getLblBeurtInfo() {
        return lblBeurtInfo;
    }

    public Label getLblKeerZesGegooid() {
        return lblKeerZesGegooid;
    }

    public Label getLblKeerZesGegooidInfo() {
        return lblKeerZesGegooidInfo;
    }

    public Label getLblBeurtNummerInfo() {
        return lblBeurtNummerInfo;
    }

    public Label getLblWarningInfo() {
        return lblWarningInfo;
    }


    public Label getLblInfoPion(int pionIndex) {
        switch (pionIndex) {
            case 1: return lblInfoPion1;
            case 2: return lblInfoPion2;
            case 3: return lblInfoPion3;
            case 4: return lblInfoPion4;
            default: throw new ParchisException("Er wordt een pionIndex van " + pionIndex +
                    " gegeven,\n het maximum is "+ ParchisModel.PIONNEN_PER_SPELER + ".\n Let wel, we starten van 1 (niet 0)");
        }
    }

    public Label getLblZetInfoPion(int pionIndex) {
        switch (pionIndex) {
            case 1: return lblZetInfoPion1;
            case 2: return lblZetInfoPion2;
            case 3: return lblZetInfoPion3;
            case 4: return lblZetInfoPion4;
            default: throw new ParchisException("Er wordt een pionIndex van " + pionIndex +
                    " gegeven,\n het maximum is "+ ParchisModel.PIONNEN_PER_SPELER + ".\n Let wel, we starten van 1 (niet 0)");
        }
    }

    public Button getBtnPion(int pionIndex) {
        switch (pionIndex) {
            case 1: return btnPion1;
            case 2: return btnPion2;
            case 3: return btnPion3;
            case 4: return btnPion4;
            default: throw new ParchisException("Er wordt een pionIndex van " + pionIndex +
                    " gegeven,\n het maximum is "+ ParchisModel.PIONNEN_PER_SPELER + ".\n Let wel, we starten van 1 (niet 0)");
        }
    }

    public TextField getTfValsSpeelLocatie() {
        return tfValsSpeelLocatie;
    }

    public TextField getTfValsSpeelDobbel() {
        return tfValsSpeelDobbel;
    }

    public Button getBtnValsSpeelPion(int pionIndex) {
        switch (pionIndex) {
            case 1: return btnValsSpeelPion1;
            case 2: return btnValsSpeelPion2;
            case 3: return btnValsSpeelPion3;
            case 4: return btnValsSpeelPion4;
            default: throw new ParchisException("Er wordt een pionIndex van " + pionIndex +
                    " gegeven,\n het maximum is "+ ParchisModel.PIONNEN_PER_SPELER + ".\n Let wel, we starten van 1 (niet 0)");
        }
    }

    public Button getBtnValsSpeelDobbel() {
        return btnValsSpeelDobbel;
    }

}
