package Parchis.view.game;

import Parchis.model.LocatieGenerator;
import javafx.geometry.Pos;
import javafx.scene.control.Label;

public class FxLocatie {
    /*
    Deze class dient puur om automatisch alle javaFx labels aan te maken voor de viewer
     */
    private final int id;
    private final int row;
    private final int col;
    private final int rowspan;
    private final int colspan;
    private Label lblLocatie;
    private static final int AANTAL_NEST = 4;
    private static final int AANTAL_RONDGANG = 68;
    private static final int AANTAL_LANDINGZONE = 28;
    private static final String CSS_NEST = "fxNest"; //om via CSS de stijl om te vormen
    private static final String CSS_RONDGANG = "fxRondgang";
    private static final String CSS_LANDINGZONE = "fxLandingzone";
    private static final String CSS_GROEN = "fxGroen";
    private static final String CSS_ROOD = "fxRood";
    private static final String CSS_BLAUW = "fxBlauw";
    private static final String CSS_GEEL = "fxGeel";
    private final int[] safeSpaces = {5, 12, 17, 22, 29, 34, 39, 46, 51, 56, 63, 68};
    private static final String CSS_SAFE_SPACE = "BtnSafeSpace"; //om via CSS de stijl om te vormen

    public FxLocatie(int id) {
        this.id = id; //++count dus de eerste instantie = nummer 1
        setLabel();
        /*
        LocatiePositie is een class die niets anders doet dan voor elke id een row, col, rowspan & colspan assigneren.
        Deze bevat methodes om met een id deze attributen op te zoeken, of om met een rij & col een id op te zoeken.
         */
        LocatieGenerator locatieGenerator = new LocatieGenerator();
        row = locatieGenerator.getRowWithId(this.id);
        col = locatieGenerator.getColWithId(this.id);
        rowspan = locatieGenerator.getRowSpanWithId(this.id);
        colspan = locatieGenerator.getColSpanWithId(this.id);

        for(int i: safeSpaces){
            if(i + AANTAL_NEST == id)
                lblLocatie.getStyleClass().add(CSS_SAFE_SPACE);
        }

        lblLocatie.setMinWidth(40);
        lblLocatie.setMinHeight(40);
        lblLocatie.setAlignment(Pos.CENTER);
    }

    private void setLabel() {
        if (this.id < AANTAL_NEST+1) { //+1 omdat we hierboven ++count doen, dus we beginnen van 1 ipv 0
            lblLocatie = new Label("Nest");
            lblLocatie.getStyleClass().add(CSS_NEST);
            if (this.id == 1) {
                lblLocatie.getStyleClass().add(CSS_BLAUW);
            } else if (this.id == 2) {
                lblLocatie.getStyleClass().add(CSS_GEEL);
            } else if (this.id == 3) {
                lblLocatie.getStyleClass().add(CSS_ROOD);
            } else {
                lblLocatie.getStyleClass().add(CSS_GROEN);
            }
        } else if (this.id < AANTAL_NEST + AANTAL_RONDGANG+1) {
            int captionId = this.id - AANTAL_NEST;
            String s = String.format("%02d", captionId); //hierdoor wordt "1" omgezet in "01" en is elke button even breed
            lblLocatie = new Label(s); // caption van de button = zijn id
            lblLocatie.getStyleClass().add(CSS_RONDGANG);
            if (captionId == 5) {
                lblLocatie.getStyleClass().add(CSS_GEEL);
            } else if (captionId == 22) {
                lblLocatie.getStyleClass().add(CSS_BLAUW);
            } else if (captionId == 39) {
                lblLocatie.getStyleClass().add(CSS_ROOD);
            } else if (captionId == 56){
                lblLocatie.getStyleClass().add(CSS_GROEN);
            }
        } else {
            // We willen de caption van die landingzone 4 x tussen 1 & 7.
            int captionId = this.id - (AANTAL_NEST + AANTAL_RONDGANG) - 1;
            int useValue = captionId % 7 + 1; //Na 7 blokken begint hij terug op 1 (8 remainer 7 = 1)
            String s = String.valueOf(useValue);
            lblLocatie = new Label(s);
            lblLocatie.getStyleClass().add(CSS_LANDINGZONE);
            if (captionId < 7) {
                lblLocatie.getStyleClass().add(CSS_GEEL);
            } else if (captionId < 14) {
                lblLocatie.getStyleClass().add(CSS_BLAUW);
           } else if (captionId < 21) {
                lblLocatie.getStyleClass().add(CSS_ROOD);
            } else {
                lblLocatie.getStyleClass().add(CSS_GROEN);
            }
        }
    }
    public Label getLbl() {
        return lblLocatie;
    }

    public int getId() {
        return id;
    }

    public int getRow() {
        return row;
    }
    public int getCol() {
        return col;
    }

    public int getRowspan() {
        return rowspan;
    }

    public int getColspan() {
        return colspan;
    }
}


