package Parchis.view.game;

import Parchis.model.*;
import Parchis.view.about.AboutPresenter;
import Parchis.view.about.AboutView;
import Parchis.view.endGame.EndGamePresenter;
import Parchis.view.endGame.EndGameView;
import Parchis.view.highscore.HighscorePresenter;
import Parchis.view.highscore.HighscoreView;
import Parchis.view.start.StartPresenter;
import Parchis.view.start.StartView;
import Parchis.view.spelregels.SpelregelsView;
import Parchis.view.spelregels.SpelregelsPresenter;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;

public class ParchisPresenter {
    private final ParchisView view;
    private final ParchisModel model;
    private Kleur kleurAanZet;
    private final LocatieGenerator locatieGenerator = new LocatieGenerator();

    public ParchisPresenter(ParchisModel model, ParchisView view) {
        this.view = view;
        this.model = model;
        //In view is gedobbeld om te zien wie begint
        kleurAanZet = view.getStartSpeler();

        model.setAantalOgen(0);//met 0 weten we dat de volgende speler nog niet gegooid heeft, wordt elke ronde terug op 0 gezet
        addEventHandlers();
    }

    private void updateView() {
        int aantalOgen = model.getAantalOgen();
        //1 uitzondering op de algemene flow: als dit de derde keer is dat je 6 gooit, word je speciaal behandeld
        //we willen hier vooral vermijden dat je aan die "updateBtnsOmTeVerzetten" komt
        if (view.getKeerZes() == 2 && aantalOgen == 6) {
            behandelDrieKeerZes();
        } else {
            boolean allePionnenOpVeld = model.allePionnenOpVeld(this.kleurAanZet);
            boolean zesGegooid = aantalOgen == 6;

            if (zesGegooid) { //toon dat dan op het scherm
                int keerZes = view.getKeerZes() + 1;
                view.getLblKeerZesGegooidInfo().setText(String.format("%dx", keerZes));
                view.setKeerZes(keerZes);
            }

            if (allePionnenOpVeld && zesGegooid) {
                //als je 6 gooit en alle pionnen staan op het veld, mag je 7 stappen zetten
                view.getLblWorpInfo().setText("uw worp: 6 + 1");
            } else {
                view.getLblWorpInfo().setText(String.format("uw worp: %d", aantalOgen));
            }

            String feedback = model.getFeedback(aantalOgen, allePionnenOpVeld);
            view.getLblWarningInfo().setText(feedback);

            updateBtnsOmTeVerzetten();
        }
    }

    private void setVolgendeSpeler() {
        //Reset de count van 6 (na 3 x is je beurt voorbij)
        view.setKeerZes(0);
        model.setAantalOgen(0); //met 0 weten we dat de volgende speler nog niet gegooid heeft
        //toon dat ook op het scherm
        view.getLblKeerZesGegooidInfo().setText(String.format("%dx", 0));
        view.getLblWorpInfo().setText(String.format("U hebt nog niet geworpen"));
        view.getLblWarningInfo().setText(String.format("U wordt verwacht de dobbelsteen te werpen"));
        for (int i = 0; i < ParchisModel.AANTAL_SPELERS; i++) {
            view.getLblZetInfoPion(i+1).setText("dobbel voor omschrijving");
        }

        // We hebben van de huidige speler een string gemaakt, dus draai dat hier om
        //https://stackoverflow.com/questions/604424/how-to-get-an-enum-value-from-a-string-value-in-java
        String strHuidigeSpeler = view.getLblBeurtInfo().getText();
        Kleur huidigeSpeler = Kleur.valueOf(strHuidigeSpeler);
        Kleur volgendeSpeler = model.getVolgendeSpeler(huidigeSpeler);
        Kleur startspeler = view.getStartSpeler();

        if (volgendeSpeler == startspeler) {
            verhoogBeurtNummer();
        }
        //Toon dan op het scherm wie nu aan zet is
        view.getLblBeurtInfo().setText(volgendeSpeler.toString());
        this.kleurAanZet = volgendeSpeler;

        updatePionLocatie();
    }

    public void behandelDrieKeerZes(){
        //Na drie keer zes gegooid, wordt de laatst verzette pion teruggezet naar zijn nest of begin van de landingzone
        //het is dan aan de volgende speler
        //één uitzondering: als de laatste verzette pion uitgespeeld is behandelen we dit met de mantel der liefde
        Pion laatsteVerzettePion = model.getLaatstVerzettePion();
        Kleur kleur = laatsteVerzettePion.getKleur();
        int pionIndex = laatsteVerzettePion.getIndex();
        Locatie huidigeLocatie = laatsteVerzettePion.getLocatie();
        ImageView image = view.getPion(kleur, pionIndex);

        if ((huidigeLocatie.getLocatieType() == LocatieType.LANDINGZONE)) {
            Locatie startLandingZone = laatsteVerzettePion.getStartRondgangLocatie();
            GridPane.setConstraints(image, startLandingZone.getColumn(), startLandingZone.getRow());
            //ook nog de pion in het model aanpassen:
            model.setPionLocatie(kleur, pionIndex, startLandingZone);
            updateBtnsOmTeDobbelen();
            //naar begin van de landingzone
        } else if ((huidigeLocatie.getLocatieType() == LocatieType.RONDGANG)){
            //naar het nest
            eetPionOp(kleur, pionIndex);
        } //else als hij in het nest zit of uitgespeeld is, doe niks
        //sowieso is het aan de volgende speler
        updateBtnsOmTeDobbelen();
        //normaal roept updateBtnsOmTeDobbelen automatisch setVolgendeSpeler op, behalve als er 6 is gegooid (zoals nu)
        //hier willen we echter wél de volgende speler zetten
        setVolgendeSpeler();
    }

    public void eetPionOp(Kleur kleur, int index) {
        //we moeten deze pion zowel visueel als in het model verzetten
        ImageView imageOpgegetenPion = view.getPion(kleur, index);
        //we hebben ook de pionFx class nodig om te weten wat zijn start rij & column is (is afhankelijk van index)
        FxPion fxPion = view.getPionFx(kleur, index);
        GridPane.setConstraints(imageOpgegetenPion, fxPion.getStartCol(), fxPion.getStartRij());
        //zet de locatie van deze pion terug op nest
        Pion opgegetenPion = model.getPion(kleur, index);
        opgegetenPion.setLocatie(opgegetenPion.getNestLocatie());
    }

    private void updatePionLocatie() {
        for (int i = 0; i < ParchisModel.AANTAL_SPELERS; i++) {
            int pionIndex = i+1;
            //Laat weten welke pion waar staat
            ImageView imagePion = view.getPion(this.kleurAanZet, pionIndex);
            Locatie locatiePion = getImageLocatie(imagePion);
            String pionLocatieInfo = getImageLocatieInfo(locatiePion);
            view.getLblInfoPion(pionIndex).setText(pionLocatieInfo);
            //Laat weten wat zijn volgende zet is
            String pionZetInfo = model.getZetInfo(this.kleurAanZet, pionIndex, model.getAantalOgen());
            view.getLblZetInfoPion(pionIndex).setText(pionZetInfo);
        }
    }

    private void verhoogBeurtNummer() {
        int huidigeBeurt = Integer.parseInt(view.getLblBeurtNummerInfo().getText());
        view.getLblBeurtNummerInfo().setText(String.format("%d", ++huidigeBeurt));
    }

    private void updateBtnsOmTeDobbelen() {
        //we gaan van "zet" modus naar "dobbel" modus
        //Laat dan toe om te dobbelen
        view.getBtnDobbel().setDisable(false);
        //Zet de pionknoppen uit
        view.getBtnPion(1).setDisable(true);
        view.getBtnPion(2).setDisable(true);
        view.getBtnPion(3).setDisable(true);
        view.getBtnPion(4).setDisable(true);
        if (model.getAantalOgen() != 6) { //7 = 6 + alle pionnen op het bord (1 zet extra)
            setVolgendeSpeler();
        }
    }
    private void updateBtnsOmTeVerzetten() {
        //we gaan van "dobbel" modus naar "zet" modus
        //Zet de dobbel knop uit
        view.getBtnDobbel().setDisable(true);

        //Zet de pionknoppen aan, als ze verzet kunnen worden
        boolean ietsMogelijk = false;
        for (int i = 0; i < ParchisModel.PIONNEN_PER_SPELER; i++) {
            int pionIndex = i+1;
            boolean kanPionVerzetten = kanPionVerzetten(view.getPion(this.kleurAanZet, pionIndex), pionIndex);
            //activeer dan de knop om hem te verzetten
            view.getBtnPion(pionIndex).setDisable(!kanPionVerzetten);
            //hou bij of één van al deze opties mogelijk was
            ietsMogelijk = ietsMogelijk || kanPionVerzetten;
        }
        if (!ietsMogelijk && model.getAantalOgen() == 6) {
            //als niks mogelijk is maar je hebt 6 gegooid, zet de knop dan terug aan
            view.getBtnDobbel().setDisable(false);
        }
        if (!ietsMogelijk && model.getAantalOgen() != 6) {
            //als niks mogelijk is en je hebt géén 6 gegooid, is je beurt voorbij
            view.getBtnDobbel().setDisable(false);
            setVolgendeSpeler();
        }
    }

    private boolean kanPionVerzetten(ImageView image, int pionIndex) {
        Locatie locatie = getImageLocatie(image);
        Kleur kleur = this.kleurAanZet;
        int worp = model.getAantalOgen();
        return model.isZetMogelijk(locatie, pionIndex, kleur, worp);
    }

    private Locatie getImageLocatie(ImageView image) {
        GridPane board = view.getBoard();
        int cIndex = board.getColumnIndex(image);
        int rIndex = board.getRowIndex(image);
        int locatieId = locatieGenerator.getIdWithRowColumn(rIndex, cIndex);
        Locatie huidigeLocatie = locatieGenerator.getLocatieWithID(locatieId);
        return huidigeLocatie;
    }

    private void setImageLocatie(Locatie locatie, Kleur kleur, int pionIndex) {
        ImageView image = view.getPion(kleur, pionIndex);
        GridPane.setConstraints(image, locatie.getColumn(), locatie.getRow());
        //ook nog de pion in het model aanpassen:
        model.setPionLocatie(kleur, pionIndex, locatie);
    }

    private String getImageLocatieInfo(Locatie locatie) {
        String s = null;
        if (locatie.getLocatieType() == LocatieType.UITGESPEELD) {
            s = "Uitgespeeld";
        } else if (locatie.getLocatieType() == LocatieType.NEST) {
            s = "Nest";
        } else if (locatie.getLocatieType() == LocatieType.RONDGANG) {
            s = "Rondgang " + locatie.getAbsoluteWaarde();
        } else {
            s = "LandingZone";
        }
        return s;
    }

    private void verzetPion(Kleur kleur, int pionIndex, ImageView image) {

        //initialiseer het aantal stappen met wat er geworpen is, dit wordt 20 als dit een zet is waarbij opgegeten wordt.
        int aantalStappen = model.getAantalOgen();

        //kijk allereerst of dit een zet is waarmee de pion uitgespeeld wordt, want dat volgt een andere flow
        Locatie testUitspeelLocatie = model.getValidatedVolgendeLocatie(kleur, pionIndex, aantalStappen);
        if (testUitspeelLocatie.getAbsoluteWaarde() == 999) { //dit wil zeggen dat de pion uitgespeeld is
            speelPionUit(pionIndex);
            //gooi dan zogezegd 10 en laat de user nog is verzetten
            model.setAantalOgen(10);
            updatePionLocatie();
            updateView();
        } else {
            boolean pionAtPionOp;

            do { //verzet de pion tot hij niks meer opeet
                //dit staat standaard op false, we bekijken later of op de nieuwe locatie een pion staat
                pionAtPionOp = false;
                Locatie nieuweLocatie = model.getValidatedVolgendeLocatie(kleur, pionIndex, aantalStappen);
                ArrayList<Pion> pionnenOpLocatie = model.getPionnenOpLocatie(nieuweLocatie);
                if (nieuweLocatie.getAbsoluteWaarde() == -999) { //dit wil zeggen dat er geen move mogelijk was
                    updateBtnsOmTeDobbelen();
                } else { //gewoon verzetten
                    if (!model.zetEetPionOp(kleur, pionIndex, aantalStappen)) {
                        GridPane.setConstraints(image, nieuweLocatie.getColumn(), nieuweLocatie.getRow());
                        //ook nog de pion in het model aanpassen:
                        model.setPionLocatie(kleur, pionIndex, nieuweLocatie);
                    } else {
                        //verzet de pion dan eerst
                        GridPane.setConstraints(image, nieuweLocatie.getColumn(), nieuweLocatie.getRow());
                        //dan ook nog in het model
                        model.setPionLocatie(kleur, pionIndex, nieuweLocatie);
                        //en verwijder dan de pion op die locatie
                        Pion opgegetenPion = pionnenOpLocatie.get(0);
                        eetPionOp(opgegetenPion.getKleur(), opgegetenPion.getIndex()); //past zowel model als fx aan
                        //nu zetten we die aantalStappen op 20 en herstarten we de loop
                        pionAtPionOp = true;
                        aantalStappen = 20;
                    }
                }
            } while (pionAtPionOp);
            //update zijn locatie
            updatePionLocatie();
            updateBtnsOmTeDobbelen();
        }
    }

    private void speelPionUit(int pionIndex) {
        Kleur kleur = this.kleurAanZet;
        Locatie uitspeelLocatie = locatieGenerator.getLocatieWithAbsoluteWaarde(999);
        ImageView image = view.getPion(kleur, pionIndex);
        //verwijder de pion
        image.setImage(null);
        //zet hem op kolom & rij 0 (=uitgespeeld)
        GridPane.setConstraints(image, 0, 0);
        //verzet zijn locatie, want anders blijft hij dummy op zijn oude locatie staan
        model.setPionLocatie(kleur, pionIndex, uitspeelLocatie);
        updatePionLocatie();
        //als dit zijn laatste pion was, is de speler gewonnen en vragen we hem zijn naam in te geven
        if (model.isSpelerGewonnen(this.kleurAanZet)) {
            endGame();
        }
    }

    private void endGame() {
        //laat de gebruiker zijn naam ingeven & toon hoeveel beurten hij nodig heeft gehad
        int beurtNummer = Integer.parseInt(view.getLblBeurtNummerInfo().getText());
        String spelerNaam = view.getSpelerNaam(this.kleurAanZet);
        EndGameView addHighscoreView = new EndGameView(beurtNummer, spelerNaam);
        EndGamePresenter endGamePresenter = new EndGamePresenter(model, addHighscoreView);
        Stage addHighscoreStage = new Stage();
        addHighscoreStage.initOwner(view.getScene().getWindow());
        addHighscoreStage.initModality(Modality.APPLICATION_MODAL);
        addHighscoreStage.setScene(new Scene(addHighscoreView));
        addHighscoreStage.setX(view.getScene().getWindow().getX() + 200);
        addHighscoreStage.setY(view.getScene().getWindow().getY() + 200);
        addHighscoreStage.showAndWait();
    }

    private void addEventHandlers() {

        view.getBtnHerbegin().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setHeaderText("Het spel afsluiten");
                alert.setContentText("Door terug te gaan verliest u onherroepelijk uw voortgang." + "\n"
                        + "Wil u zeker herbeginnen?");
                alert.setTitle("Opgelet");
                alert.getButtonTypes().clear();
                ButtonType neen = new ButtonType("Nee, speel voort");
                ButtonType ja = new ButtonType("Ja, herbegin het spel");
                alert.getButtonTypes().addAll(neen, ja);
                alert.showAndWait();

                if (alert.getResult() == null || alert.getResult().equals(neen)) {
                    event.consume();
                } else {
                    StartView startView = new StartView();
                    StartPresenter startPresenter = new StartPresenter(model, startView);
                    view.getScene().setRoot(startView);
                    startView.getScene().getWindow().sizeToScene();
                }
            }
        });
        view.getBtnDobbel().setOnAction(event -> {
            model.werp();
            updatePionLocatie();
            updateView();
        }
        );

        view.getBtnValsSpeelDobbel().setOnAction(event -> {
            //in plaats van random worp te doen, overschrijven we de worp
            int valsSpeelWorp = Integer.parseInt(view.getTfValsSpeelDobbel().getText());
            model.setAantalOgen(valsSpeelWorp);
            updatePionLocatie();
            updateView();
                }
        );

        //knoppen definieren om de pion te verzetten
        for (int i = 0; i < ParchisModel.AANTAL_SPELERS; i++) {
            int pionIndex = i+1;
            try {
                view.getBtnPion(pionIndex).setOnAction( ( e ) ->
                {
                    ImageView image = view.getPion(this.kleurAanZet, pionIndex);
                    verzetPion(this.kleurAanZet, pionIndex, image);
                });
            } catch (ParchisException p) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Pion index out of range");
                alert.setContentText(p.getMessage());
                alert.showAndWait();
            }
        }

        //knoppen definieren om vals te spelen
        for (int i = 0; i < ParchisModel.AANTAL_SPELERS; i++) {
            int pionIndex = i+1;
            try {
                view.getBtnValsSpeelPion(pionIndex).setOnAction( ( e ) ->
                {
                    ImageView image = view.getPion(this.kleurAanZet, pionIndex);
                    int absoluteWaarde = Integer.parseInt(view.getTfValsSpeelLocatie().getText());

                    if (absoluteWaarde == 999) { //dan wou de speler hem eigenlijk uitspelen
                        speelPionUit(pionIndex);
                        //er wordt dan zogezegd een gratis worp van 10 toegekend
                        model.setAantalOgen(10);
                        updatePionLocatie();
                        updateView();
                    } else {
                        Locatie valsSpeelLocatie = locatieGenerator.getLocatieWithAbsoluteWaarde(absoluteWaarde);
                        //als we verzetten naar een locatie met een pion op, verwijder deze dan
                        ArrayList<Pion> pionnenOpLocatie = model.getPionnenOpLocatie(valsSpeelLocatie);
                        if (pionnenOpLocatie.size() == 1 && this.kleurAanZet != pionnenOpLocatie.get(0).getKleur()){
                            Pion opgegetenPion = pionnenOpLocatie.get(0);
                            eetPionOp(opgegetenPion.getKleur(), opgegetenPion.getIndex()); //past zowel model als fx aan
                        }
                        GridPane.setConstraints(image, valsSpeelLocatie.getColumn(), valsSpeelLocatie.getRow());
                        //ook nog de pion in het model aanpassen:
                        model.setPionLocatie(this.kleurAanZet, pionIndex, valsSpeelLocatie);
                        updatePionLocatie();
                    }
                });
            } catch (ParchisException p) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Pion index out of range");
                alert.setContentText(p.getMessage());
                alert.showAndWait();
            }
        }
        view.getMiSpelregels().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                SpelregelsView spelregelsView = new SpelregelsView();
                SpelregelsPresenter spelregelsPresenter = new SpelregelsPresenter(spelregelsView);
                Stage spelregelsStage = new Stage();
                spelregelsStage.setTitle("Parchis spelregels");
                spelregelsStage.initOwner(view.getScene().getWindow());
                spelregelsStage.initModality(Modality.APPLICATION_MODAL);
                spelregelsStage.setScene(new Scene(spelregelsView));
                spelregelsStage.setX(view.getScene().getWindow().getX() + 200);
                spelregelsStage.setY(view.getScene().getWindow().getY() + 200);
                spelregelsStage.showAndWait();
            }
        });

        view.getMiAbout().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                AboutView aboutView = new AboutView();
                AboutPresenter aboutPresenter = new AboutPresenter(aboutView);
                Stage aboutStage = new Stage();
                aboutStage.setTitle("Wat info over uw programmeur");
                aboutStage.initOwner(view.getScene().getWindow());
                aboutStage.initModality(Modality.APPLICATION_MODAL);
                aboutStage.setScene(new Scene(aboutView));
                aboutStage.setX(view.getScene().getWindow().getX() + 200);
                aboutStage.setY(view.getScene().getWindow().getY() + 200);
                aboutStage.showAndWait();
            }
        });

        view.getMiHighscore().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                HighscoreView highscoreView = new HighscoreView();
                HighscorePresenter highscorePresenter = new HighscorePresenter(model, highscoreView);
                Stage highscoreStage = new Stage();
                highscoreStage.setTitle("de top scores");
                highscoreStage.setScene(new Scene(highscoreView));
                highscoreStage.initOwner(view.getScene().getWindow());
                highscoreStage.initModality(Modality.APPLICATION_MODAL);
                highscoreStage.setX(view.getScene().getWindow().getX() + 200);
                highscoreStage.setY(view.getScene().getWindow().getY() + 200);
                highscoreStage.showAndWait();
            }
        });
        view.getBtnWinSpel().setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                endGame();
            }
        });
    }
}