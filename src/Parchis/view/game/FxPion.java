package Parchis.view.game;

import Parchis.model.Kleur;
import Parchis.model.ParchisException;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class FxPion {
    private final Kleur kleur;
    private final int index;
    private final int startRij;
    private final int startCol;
    private final ImageView image;

    public FxPion(Kleur kleur, int index, int startRij, int startCol) {
        this.kleur = kleur;
        this.index = index;
        this.startRij = startRij;
        this.startCol = startCol;

        switch (kleur) {
            case ROOD: image = new ImageView( new Image("/icons/chess-pawn-red.png")); break;
            case GEEL: image = new ImageView( new Image("/icons/chess-pawn-yellow.png")); break;
            case GROEN: image = new ImageView( new Image("/icons/chess-pawn-green.png")); break;
            case BLAUW: image = new ImageView( new Image("/icons/chess-pawn-blue.png")); break;
            default: throw new ParchisException("De volgende kleur wordt doorgegeven: " + kleur +
                    ". \n Ofwel bestaat deze niet ofwel moet deze functie nog aangepast worden.\n");
        }
    }

    public Kleur getKleur() {
        return kleur;
    }

    public int getIndex() {
        return index;
    }

    public int getStartRij() {
        return startRij;
    }

    public int getStartCol() {
        return startCol;
    }

    public ImageView getImage() {
        return image;
    }
}
