package Parchis.view.game;

import Parchis.model.Kleur;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class FxPionGenerator {
    Map<Integer, FxPion> pionnenFx = new HashMap<>();

    public FxPionGenerator() {
        populatePionFx();
    }

    private void populatePionFx() {
        //hier kunnen we niet loopen, omdat die start rij & kolom niet afgeleid kunnen worden
        pionnenFx.put(1, new FxPion(Kleur.ROOD, 1, 15, 3));
        pionnenFx.put(2, new FxPion(Kleur.ROOD, 2, 16, 3));
        pionnenFx.put(3, new FxPion(Kleur.ROOD, 3, 15, 4));
        pionnenFx.put(4, new FxPion(Kleur.ROOD, 4, 16, 4));
        pionnenFx.put(5, new FxPion(Kleur.GEEL, 1, 3, 14));
        pionnenFx.put(6, new FxPion(Kleur.GEEL, 2, 4, 14));
        pionnenFx.put(7, new FxPion(Kleur.GEEL, 3, 3, 15));
        pionnenFx.put(8, new FxPion(Kleur.GEEL, 4, 4, 15));
        pionnenFx.put(9, new FxPion(Kleur.BLAUW, 1, 3, 3));
        pionnenFx.put(10, new FxPion(Kleur.BLAUW, 2, 4, 3));
        pionnenFx.put(11, new FxPion(Kleur.BLAUW, 3, 3, 4));
        pionnenFx.put(12, new FxPion(Kleur.BLAUW, 4, 4, 4));
        pionnenFx.put(13, new FxPion(Kleur.GROEN, 1, 15, 14));
        pionnenFx.put(14, new FxPion(Kleur.GROEN, 2, 16, 14));
        pionnenFx.put(15, new FxPion(Kleur.GROEN, 3, 15, 15));
        pionnenFx.put(16, new FxPion(Kleur.GROEN, 4, 16, 15));
    }

    public FxPion getPionFxWithID(int id) {
        Iterator it = pionnenFx.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if ((int) pair.getKey() == id) {
                return pionnenFx.get(pair.getKey());
            }
            //it.remove(); // avoids a ConcurrentModificationException
        }
        return new FxPion(Kleur.ROOD, 1, 15, 3);
    }

    public FxPion getPionFxWithKleurIndex(Kleur kleur, int index) {
        Iterator it = pionnenFx.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            FxPion fxPion = pionnenFx.get(pair.getKey());
            int objectIndex = fxPion.getIndex();
            Kleur objectKleur = fxPion.getKleur();
            if ((index == objectIndex) && (kleur == objectKleur)) {
                return pionnenFx.get(pair.getKey());
            }
            //it.remove(); // avoids a ConcurrentModificationException
        }
        return new FxPion(Kleur.ROOD, 1, 15, 3);
    }

    public Map<Integer, FxPion> getPionnenFx() {
        return pionnenFx;
    }
}
