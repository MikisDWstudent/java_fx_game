package Parchis.view.game;

import Parchis.model.Kleur;
import Parchis.model.ParchisException;
import Parchis.view.start.StartView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import Parchis.view.start.StartView;

import java.util.*;

public class ParchisView extends ScrollPane {
    private MenuItem miSpelregels;
    private MenuItem miHighscore;
    private MenuItem miAbout;
    private Button btnHerbegin;
    private Button btnWinSpel;
    private int keerZes;
    private int beurtNummer;
    private GridInfo zijMenu;
    private GridBoard speelBord;
    private final Kleur startSpeler;
    private int score;
    private Region spacingBtwnBtns;
    private final StartView startView;

    public ParchisView(StartView startView) { //startview om een referentie te maken aan cheatmodus
        // random getal tussen 1 & 4 bepaalt wie begint
        Random random = new Random();
        this.startView = startView;
        startSpeler = Kleur.values()[random.nextInt(4)];
        beurtNummer = 0;
        initialiseNodes();
        layoutNodes();
        //als cheat modus aan staat, zet dan de nodige knoppen op actief
        boolean cheatModusOn = startView.getCheatModus().isSelected();
        toggleCheatModus(cheatModusOn);

    }

    private void initialiseNodes() {
        miSpelregels = new MenuItem("Spelregels");
        miHighscore = new MenuItem("Highscore");
        miAbout = new MenuItem("About");
        btnHerbegin = new Button("Terug naar startscherm");
        btnWinSpel = new Button("Win spel voortijdig");
        spacingBtwnBtns = new Region();
    }

    private void layoutNodes() {
        BorderPane bp = new BorderPane();
        Menu mnHelp = new Menu("Menu");
        mnHelp.getItems().addAll(miSpelregels, miHighscore, miAbout);
        MenuBar menuBar = new MenuBar(mnHelp);

        zijMenu = new GridInfo(startSpeler, startView);
        GridPane info = zijMenu.getInfo();

        speelBord = new GridBoard();
        GridPane board = speelBord.getBoard();

        HBox navMenu = new HBox();
        HBox.setHgrow(spacingBtwnBtns, Priority.ALWAYS);
        navMenu.setPadding(new Insets(15, 12, 15, 12));
        navMenu.setSpacing(10);
        navMenu.getChildren().addAll(btnHerbegin, spacingBtwnBtns, btnWinSpel);

        //Left gebruiken we momenteel nog niet
        bp.setTop(menuBar);
        bp.setCenter(board);
        bp.setRight(info);
        bp.setBottom(navMenu);

        this.setContent(bp);
        this.setPannable(true);

        info.setAlignment(Pos.CENTER);
        BorderPane.setMargin(btnHerbegin, new Insets(10, 10, 10, 10));
        BorderPane.setAlignment(btnHerbegin, Pos.BOTTOM_LEFT);

        this.setPrefSize(1500, 850);
        //board.setGridLinesVisible(true);
        //info.setGridLinesVisible(true);
    }

    Button getBtnHerbegin() {
        return btnHerbegin;
    }

    MenuItem getMiSpelregels() {
        return miSpelregels;
    }
    MenuItem getMiHighscore() {
        return miHighscore;
    }
    MenuItem getMiAbout() {
        return miAbout;
    }

    Button getBtnWinSpel() {
        return btnWinSpel;
    }

    Button getBtnDobbel() {
        return zijMenu.getBtnDobbel();
    }

    Label getLblWorpInfo() {
        return zijMenu.getLblWorpInfo();
    }

    int getKeerZes() {
        return keerZes;
    }

    void setKeerZes(int keerZes) {
        this.keerZes = keerZes;
    }

    Label getLblKeerZesGegooidInfo() {
        return zijMenu.getLblKeerZesGegooidInfo();
    }

    Label getLblWarningInfo() {
        return zijMenu.getLblWarningInfo();
    }

    Label getLblInfoPion(int index) {
        return zijMenu.getLblInfoPion(index);
    }

    Label getLblZetInfoPion(int index) {
        return zijMenu.getLblZetInfoPion(index);
    }

    Button getBtnPion(int pionIndex) {
        return zijMenu.getBtnPion(pionIndex);
    }

    Label getLblBeurtInfo() {
        return zijMenu.getLblBeurtInfo();
    }

    GridPane getBoard() {
        return speelBord.getBoard();
    }

    FxPion getPionFx(Kleur kleur, int index) {
        return speelBord.getPionFxGenerator().getPionFxWithKleurIndex(kleur, index);
    }

    ImageView getPion(Kleur kleur, int index) {
        return speelBord.getPionFxGenerator().getPionFxWithKleurIndex(kleur, index).getImage();
    }

    Kleur getStartSpeler() {
        //We moeten aan de start speler kunnen om te bepalen wanneer beurt1 voorbij is
        return startSpeler;
    }

    Label getLblBeurtNummerInfo() {
        return zijMenu.getLblBeurtNummerInfo();
    }

    Button getBtnValsSpeelPion(int pionIndex) {
        return zijMenu.getBtnValsSpeelPion(pionIndex);
    }

    TextField getTfValsSpeelLocatie() {
        return zijMenu.getTfValsSpeelLocatie();
    }

    Button getBtnValsSpeelDobbel() {
        return zijMenu.getBtnValsSpeelDobbel();
    }

    TextField getTfValsSpeelDobbel() {
        return zijMenu.getTfValsSpeelDobbel();
    }

    int getScore() { return score; }
    void setScore(int score) { this.score = score; }

    private void toggleCheatModus(boolean cheatModusEnabled) {
        btnWinSpel.setVisible(cheatModusEnabled);
    }

    String getSpelerNaam(Kleur kleur) {
        switch (kleur) {
            case GEEL: return startView.getTfGeleSpeler().getText();
            case BLAUW: return startView.getTfBlauweSpeler().getText();
            case ROOD: return startView.getTfRodeSpeler().getText();
            case GROEN: return startView.getTfGroeneSpeler().getText();
            default: throw new ParchisException("De volgende kleur wordt doorgegeven: " + kleur +
                    ". \n Ofwel bestaat deze niet ofwel moet deze functie nog aangepast worden.\n");
        }
    }

}