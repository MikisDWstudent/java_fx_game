package Parchis.view.game;

import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class GridBoard {

    private GridPane board;
    List<FxLocatie> fxLocatieLijst = new ArrayList<>();
    private final FxPionGenerator fxPionGenerator = new FxPionGenerator();

    public GridBoard() {
        layoutNodes();
    }

    private void layoutNodes() {
        board = new GridPane();
        board.getStyleClass().add("board"); //zo kunnen we via css hier aan
        //Plaats de locaties op het scherm, de kolom & de rij worden berekend in de class zelf
        for (int i = 0; i < 100; i++) { //4 nesten, 68 rondgangen & 28 landingzones
            FxLocatie locatiefx = new FxLocatie(i+1);
            fxLocatieLijst.add(locatiefx); //Zodat we hier over kunnen loopen later
            Label label = locatiefx.getLbl();
            int row = locatiefx.getRow();
            int col = locatiefx.getCol();
            int rowSpan = locatiefx.getRowspan();
            int colSpan = locatiefx.getColspan();
            board.add(label, col, row, rowSpan, colSpan);
        }

        //plaats alle pionnen op het board
        Map<Integer, FxPion> pionnenFx = fxPionGenerator.getPionnenFx();
        Iterator it = pionnenFx.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            FxPion fxPion = pionnenFx.get(pair.getKey());
            board.add(fxPion.getImage(), fxPion.getStartCol(), fxPion.getStartRij());
        }

    }

    public GridPane getBoard() {
        return board;
    }

    public List<FxLocatie> getLocatieFxLijst() {
        return fxLocatieLijst;
    }

    public FxPionGenerator getPionFxGenerator() {
        return fxPionGenerator;
    }
}
