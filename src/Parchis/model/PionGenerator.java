package Parchis.model;

public class PionGenerator {
    Pion[] pionnen = new Pion[ParchisModel.AANTAL_SPELERS * ParchisModel.PIONNEN_PER_SPELER];

    public PionGenerator() {
        for (int i = 0; i < ParchisModel.AANTAL_SPELERS; i++) {
            for (int j = 0; j < ParchisModel.PIONNEN_PER_SPELER; j++) {
                int index = j + (ParchisModel.AANTAL_SPELERS * i);
                pionnen[index] = new Pion(Kleur.values()[i], j+1);
            }
        }
    }

    public Pion getPionKleurIndex(Kleur kleur, int index) {
        for (Pion pion : pionnen) {
            if ((kleur == pion.getKleur()) && (index == pion.getIndex()))
                return pion;
        }
        throw new ParchisException("Er wordt gevraagd voor een pion met kleur " + kleur +
                " en index " + index + ".\n Deze pion lijkt niet te bestaan.\n");
    }
}