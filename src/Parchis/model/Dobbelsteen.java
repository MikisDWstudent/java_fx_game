package Parchis.model;

import java.util.Random;

public class Dobbelsteen {

    public static final int MAX_AANTAL_OGEN = 6;
    private final Random random;

    public Dobbelsteen() {
        this.random = new Random();
    }

    public int werp(){
        return  random.nextInt(MAX_AANTAL_OGEN) + 1;
    }
}
