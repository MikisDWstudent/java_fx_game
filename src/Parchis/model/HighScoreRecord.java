package Parchis.model;

public class HighScoreRecord implements Comparable<HighScoreRecord> {
    private String naam;
    private int aantalBeurten;

    public HighScoreRecord(String naam, int aantalBeurten) {
        this.naam = naam;
        this.aantalBeurten = aantalBeurten;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public int getAantalBeurten() {
        return aantalBeurten;
    }

    public void setAantalBeurten(int aantalBeurten) {
        this.aantalBeurten = aantalBeurten;
    }

    @Override
    public int compareTo(HighScoreRecord o) {
        return Integer.compare(this.aantalBeurten, o.aantalBeurten);
    }

    @Override
    public String toString() {
        return String.format("%s,%d", naam, aantalBeurten);
    }
}
