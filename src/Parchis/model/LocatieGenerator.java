package Parchis.model;

import Parchis.view.game.FxPion;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
/*
Als een speler een pion dropt op een bepaalde rij & locatie, dan correspondeert dat met een bepaalde locatie.
Die locatie heeft altijd een "ID", en deze class laat toe om tussen de twee te switchen. Je kan hier een rij & kolom
ingeven en een ID terugkrijgen, maar ook andersom.
 */
public class LocatieGenerator {
    Map<Integer, Locatie> mp = new HashMap<>();
    java.util.List<java.util.Map.Entry<Integer,Integer>> startRowCol= new java.util.ArrayList<>();

    public LocatieGenerator() {
        populateLocatiePositie();
    }

    private void populateLocatiePositie(){
        mp.put(1, new Locatie(3,3,3,3,-3, LocatieType.NEST));
        mp.put(2, new Locatie(3,14,3,3,-2, LocatieType.NEST));
        mp.put(3, new Locatie(15,3,3,3,-1, LocatieType.NEST));
        mp.put(4, new Locatie(15,14,3,3,0, LocatieType.NEST));
        mp.put(5, new Locatie(9,19,1,1,1, LocatieType.RONDGANG));
        mp.put(6, new Locatie(9,18,1,1,2, LocatieType.RONDGANG));
        mp.put(7, new Locatie(9,17,1,1,3, LocatieType.RONDGANG));
        mp.put(8, new Locatie(9,16,1,1,4, LocatieType.RONDGANG));
        mp.put(9, new Locatie(9,15,1,1,5, LocatieType.RONDGANG));
        mp.put(10, new Locatie(9,14,1,1,6, LocatieType.RONDGANG));
        mp.put(11, new Locatie(9,13,1,1,7, LocatieType.RONDGANG));
        mp.put(12, new Locatie(9,12,1,1,8, LocatieType.RONDGANG));
        mp.put(13, new Locatie(8,11,1,1,9, LocatieType.RONDGANG));
        mp.put(14, new Locatie(7,11,1,1,10, LocatieType.RONDGANG));
        mp.put(15, new Locatie(6,11,1,1,11, LocatieType.RONDGANG));
        mp.put(16, new Locatie(5,11,1,1,12, LocatieType.RONDGANG));
        mp.put(17, new Locatie(4,11,1,1,13, LocatieType.RONDGANG));
        mp.put(18, new Locatie(3,11,1,1,14, LocatieType.RONDGANG));
        mp.put(19, new Locatie(2,11,1,1,15, LocatieType.RONDGANG));
        mp.put(20, new Locatie(1,11,1,1,16, LocatieType.RONDGANG));
        mp.put(21, new Locatie(1,10,1,1,17, LocatieType.RONDGANG));
        mp.put(22, new Locatie(1,9,1,1,18, LocatieType.RONDGANG));
        mp.put(23, new Locatie(2,9,1,1,19, LocatieType.RONDGANG));
        mp.put(24, new Locatie(3,9,1,1,20, LocatieType.RONDGANG));
        mp.put(25, new Locatie(4,9,1,1,21, LocatieType.RONDGANG));
        mp.put(26, new Locatie(5,9,1,1,22, LocatieType.RONDGANG));
        mp.put(27, new Locatie(6,9,1,1,23, LocatieType.RONDGANG));
        mp.put(28, new Locatie(7,9,1,1,24, LocatieType.RONDGANG));
        mp.put(29, new Locatie(8,9,1,1,25, LocatieType.RONDGANG));
        mp.put(30, new Locatie(9,8,1,1,26, LocatieType.RONDGANG));
        mp.put(31, new Locatie(9,7,1,1,27, LocatieType.RONDGANG));
        mp.put(32, new Locatie(9,6,1,1,28, LocatieType.RONDGANG));
        mp.put(33, new Locatie(9,5,1,1,29, LocatieType.RONDGANG));
        mp.put(34, new Locatie(9,4,1,1,30, LocatieType.RONDGANG));
        mp.put(35, new Locatie(9,3,1,1,31, LocatieType.RONDGANG));
        mp.put(36, new Locatie(9,2,1,1,32, LocatieType.RONDGANG));
        mp.put(37, new Locatie(9,1,1,1,33, LocatieType.RONDGANG));
        mp.put(38, new Locatie(10,1,1,1,34, LocatieType.RONDGANG));
        mp.put(39, new Locatie(11,1,1,1,35, LocatieType.RONDGANG));
        mp.put(40, new Locatie(11,2,1,1,36, LocatieType.RONDGANG));
        mp.put(41, new Locatie(11,3,1,1,37, LocatieType.RONDGANG));
        mp.put(42, new Locatie(11,4,1,1,38, LocatieType.RONDGANG));
        mp.put(43, new Locatie(11,5,1,1,39, LocatieType.RONDGANG));
        mp.put(44, new Locatie(11,6,1,1,40, LocatieType.RONDGANG));
        mp.put(45, new Locatie(11,7,1,1,41, LocatieType.RONDGANG));
        mp.put(46, new Locatie(11,8,1,1,42, LocatieType.RONDGANG));
        mp.put(47, new Locatie(12,9,1,1,43, LocatieType.RONDGANG));
        mp.put(48, new Locatie(13,9,1,1,44, LocatieType.RONDGANG));
        mp.put(49, new Locatie(14,9,1,1,45, LocatieType.RONDGANG));
        mp.put(50, new Locatie(15,9,1,1,46, LocatieType.RONDGANG));
        mp.put(51, new Locatie(16,9,1,1,47, LocatieType.RONDGANG));
        mp.put(52, new Locatie(17,9,1,1,48, LocatieType.RONDGANG));
        mp.put(53, new Locatie(18,9,1,1,49, LocatieType.RONDGANG));
        mp.put(54, new Locatie(19,9,1,1,50, LocatieType.RONDGANG));
        mp.put(55, new Locatie(19,10,1,1,51, LocatieType.RONDGANG));
        mp.put(56, new Locatie(19,11,1,1,52, LocatieType.RONDGANG));
        mp.put(57, new Locatie(18,11,1,1,53, LocatieType.RONDGANG));
        mp.put(58, new Locatie(17,11,1,1,54, LocatieType.RONDGANG));
        mp.put(59, new Locatie(16,11,1,1,55, LocatieType.RONDGANG));
        mp.put(60, new Locatie(15,11,1,1,56, LocatieType.RONDGANG));
        mp.put(61, new Locatie(14,11,1,1,57, LocatieType.RONDGANG));
        mp.put(62, new Locatie(13,11,1,1,58, LocatieType.RONDGANG));
        mp.put(63, new Locatie(12,11,1,1,59, LocatieType.RONDGANG));
        mp.put(64, new Locatie(11,12,1,1,60, LocatieType.RONDGANG));
        mp.put(65, new Locatie(11,13,1,1,61, LocatieType.RONDGANG));
        mp.put(66, new Locatie(11,14,1,1,62, LocatieType.RONDGANG));
        mp.put(67, new Locatie(11,15,1,1,63, LocatieType.RONDGANG));
        mp.put(68, new Locatie(11,16,1,1,64, LocatieType.RONDGANG));
        mp.put(69, new Locatie(11,17,1,1,65, LocatieType.RONDGANG));
        mp.put(70, new Locatie(11,18,1,1,66, LocatieType.RONDGANG));
        mp.put(71, new Locatie(11,19,1,1,67, LocatieType.RONDGANG));
        mp.put(72, new Locatie(10,19,1,1,68, LocatieType.RONDGANG));
        //gele rondgang
        mp.put(73, new Locatie(10,18,1,1,69, LocatieType.LANDINGZONE));
        mp.put(74, new Locatie(10,17,1,1,70, LocatieType.LANDINGZONE));
        mp.put(75, new Locatie(10,16,1,1,71, LocatieType.LANDINGZONE));
        mp.put(76, new Locatie(10,15,1,1,72, LocatieType.LANDINGZONE));
        mp.put(77, new Locatie(10,14,1,1,73, LocatieType.LANDINGZONE));
        mp.put(78, new Locatie(10,13,1,1,74, LocatieType.LANDINGZONE));
        mp.put(79, new Locatie(10,12,1,1,75, LocatieType.LANDINGZONE));
        //blauwe rondgang (sprong van 17 tov geel, want blauw start op 22 ipv 5)
        mp.put(80, new Locatie(2,10,1,1,86, LocatieType.LANDINGZONE));
        mp.put(81, new Locatie(3,10,1,1,87, LocatieType.LANDINGZONE));
        mp.put(82, new Locatie(4,10,1,1,88, LocatieType.LANDINGZONE));
        mp.put(83, new Locatie(5,10,1,1,89, LocatieType.LANDINGZONE));
        mp.put(84, new Locatie(6,10,1,1,90, LocatieType.LANDINGZONE));
        mp.put(85, new Locatie(7,10,1,1,91, LocatieType.LANDINGZONE));
        mp.put(86, new Locatie(8,10,1,1,92, LocatieType.LANDINGZONE));
        //rode rondgang (sprong van 34 tov geel, want rood start op 39 ipv 5)
        mp.put(87, new Locatie(10,2,1,1,103, LocatieType.LANDINGZONE));
        mp.put(88, new Locatie(10,3,1,1,104, LocatieType.LANDINGZONE));
        mp.put(89, new Locatie(10,4,1,1,105, LocatieType.LANDINGZONE));
        mp.put(90, new Locatie(10,5,1,1,106, LocatieType.LANDINGZONE));
        mp.put(91, new Locatie(10,6,1,1,107, LocatieType.LANDINGZONE));
        mp.put(92, new Locatie(10,7,1,1,108, LocatieType.LANDINGZONE));
        mp.put(93, new Locatie(10,8,1,1,109, LocatieType.LANDINGZONE));
        //groene rondgang (sprong van 51 tov geel, want groen start op 56 ipv 5)
        mp.put(94, new Locatie(18,10,1,1,120, LocatieType.LANDINGZONE));
        mp.put(95, new Locatie(17,10,1,1,121, LocatieType.LANDINGZONE));
        mp.put(96, new Locatie(16,10,1,1,122, LocatieType.LANDINGZONE));
        mp.put(97, new Locatie(15,10,1,1,123, LocatieType.LANDINGZONE));
        mp.put(98, new Locatie(14,10,1,1,124, LocatieType.LANDINGZONE));
        mp.put(99, new Locatie(13,10,1,1,125, LocatieType.LANDINGZONE));
        mp.put(100, new Locatie(12,10,1,1,126, LocatieType.LANDINGZONE));
        //één dummy locatie om naar te verwijzen als een pion uitgespeeld is
        mp.put(101, new Locatie(0,0,0,0,999, LocatieType.UITGESPEELD));
    }

    private Boolean isStartLocatie(int row, int col) {
        //vervelend genoeg floaten de pionnen eigenlijk over hun nest, en pakken ze 4 plekken in
        //een nest is één plaats met row & col span = 2, dit hieronder zijn alle effectieve startposities
        if (row == 15 && col == 3) {
            return true;
        } else if (row == 16 && col == 3) {
            return true;
        } else if (row == 15 && col == 4) {
            return true;
        } else if (row == 16 && col == 4) {
            return true;
        } else if (row == 3 && col == 14) {
            return true;
        } else if (row == 4 && col == 14) {
            return true;
        } else if (row == 3 && col == 15) {
            return true;
        } else if (row == 4 && col == 15) {
            return true;
        } else if (row == 3 && col == 3) {
            return true;
        } else if (row == 4 && col == 3) {
            return true;
        } else if (row == 3 && col == 4) {
            return true;
        } else if (row == 4 && col == 4) {
            return true;
        } else if (row == 15 && col == 14) {
            return true;
        } else if (row == 16 && col == 14) {
            return true;
        } else if (row == 15 && col == 15) {
            return true;
        } else return row == 16 && col == 15;
    }

    public Locatie getLocatieWithID(int id) {
        //één uitzondering: als die id -1 is, is dat een startlocatie id. Dan geven we een dummy nest terug
        if (id == -1) {
            //dan geven we een dummy terug
            return new Locatie(0,0,0,0,0, LocatieType.NEST);
        }
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if ((int) pair.getKey() == id) {
                return mp.get(pair.getKey());
            }
            //it.remove(); // avoids a ConcurrentModificationException
        }
        throw new ParchisException("We vinden geen locatie met de volgende id: " + id);
    }

    public Locatie getLocatieWithAbsoluteWaarde(int absoluteWaarde) {
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Locatie locatie = mp.get(pair.getKey());
            int objectAbsoluteWaarde = locatie.getAbsoluteWaarde();
            if (objectAbsoluteWaarde == absoluteWaarde) {
                return mp.get(pair.getKey());
            }
            //it.remove(); // avoids a ConcurrentModificationException
        }
        throw new ParchisException("We vinden geen locatie met absolute waarde " + absoluteWaarde);
    }
    public Locatie getLocatieWithRelatieveWaarde(int relatieveWaarde, Kleur kleur) {
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Locatie locatie = mp.get(pair.getKey());
            int objectRelatieveWaarde = locatie.getRelatievewaarde(kleur);
            if (objectRelatieveWaarde == relatieveWaarde) {
                return mp.get(pair.getKey());
            }
            //it.remove(); // avoids a ConcurrentModificationException
        }
        throw new ParchisException("We vinden geen locatie met relatieve waarde " + relatieveWaarde + " voor kleur " + kleur);
    }

    public int getIdWithRowColumn(int row, int column) {
        //pionnen floaten eigenlijk over hun nest, dus die zitten niet in deze lijst
        if (isStartLocatie(row, column)) {
            //dan geven we een dummy terug
            return -1;
        }

        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Locatie locatie = mp.get(pair.getKey());

            int objectRij = locatie.getRow();
            int objectColumn = locatie.getColumn();
            if (row == objectRij && column == objectColumn) {
                return (int) pair.getKey();
            }
            //it.remove(); // avoids a ConcurrentModificationException
        }
        throw new ParchisException("We vinden geen locatie met rij " + row + " en kolom " + column);
    }
    public int getRowWithId(int id) {
        //één uitzondering: als die id -1 is, is dat een startlocatie id. Dan geven we een dummy 0 terug
        if (id == -1) {
            //dan geven we een dummy terug
            return 0;
        }
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if ((int) pair.getKey() == id) {
                Locatie locatie = mp.get(pair.getKey());
                return locatie.getRow();
            }
            //it.remove(); // avoids a ConcurrentModificationException
        }
        throw new ParchisException("We vinden geen locatie met de volgende id: " + id);
    }
    public int getColWithId(int id) {
        //één uitzondering: als die id -1 is, is dat een startlocatie id. Dan geven we een dummy 0 terug
        if (id == -1) {
            //dan geven we een dummy terug
            return 0;
        }
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if ((int) pair.getKey() == id) {
                Locatie locatie = mp.get(pair.getKey());
                return locatie.getColumn();
            }
            //it.remove(); // avoids a ConcurrentModificationException
        }
        throw new ParchisException("We vinden geen locatie met de volgende id: " + id);
    }
    public int getRowSpanWithId(int id) {
        //één uitzondering: als die id -1 is, is dat een startlocatie id. Dan geven we een dummy 0 terug
        if (id == -1) {
            //dan geven we een dummy terug
            return 0;
        }
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if ((int) pair.getKey() == id) {
                Locatie locatie = mp.get(pair.getKey());
                return locatie.getRowSpan();
            }
            //it.remove(); // avoids a ConcurrentModificationException
        }
        throw new ParchisException("We vinden geen locatie met de volgende id: " + id);
    }
    public int getColSpanWithId(int id) {
        //één uitzondering: als die id -1 is, is dat een startlocatie id. Dan geven we een dummy 0 terug
        if (id == -1) {
            //dan geven we een dummy terug
            return 0;
        }
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if ((int) pair.getKey() == id) {
                Locatie locatie = mp.get(pair.getKey());
                return locatie.getColSpan();
            }
            //it.remove(); // avoids a ConcurrentModificationException
        }
        throw new ParchisException("We vinden geen locatie met de volgende id: " + id);
    }
    public Locatie getStartLocatie(Kleur kleur) {
        switch (kleur) {
            case GEEL: return getLocatieWithAbsoluteWaarde(5);
            case BLAUW: return getLocatieWithAbsoluteWaarde(22);
            case ROOD: return getLocatieWithAbsoluteWaarde(39);
            case GROEN: return getLocatieWithAbsoluteWaarde(56);
            default: throw new ParchisException("De volgende kleur wordt doorgegeven: " + kleur +
                    ". \n Ofwel bestaat deze niet ofwel moet deze functie nog aangepast worden.\n");
        }
    }
    public Locatie getStartLandingZoneLocatie(Kleur kleur) { //de eerste locatie van de landing zone
        switch (kleur) {
            case GEEL: return getLocatieWithAbsoluteWaarde(69);
            case BLAUW: return getLocatieWithAbsoluteWaarde(86);
            case ROOD: return getLocatieWithAbsoluteWaarde(103);
            case GROEN: return getLocatieWithAbsoluteWaarde(120);
            default: throw new ParchisException("De volgende kleur wordt doorgegeven: " + kleur +
                    ". \n Ofwel bestaat deze niet ofwel moet deze functie nog aangepast worden.\n");
        }
    }

    public Locatie getLocatieMaxAbsoluteWaardeKleur(Kleur kleur) { //de locatie met de hoogste absolute waarde per kleur
        switch (kleur) {
            case GEEL: return getLocatieWithAbsoluteWaarde(75);
            case BLAUW: return getLocatieWithAbsoluteWaarde(92);
            case ROOD: return getLocatieWithAbsoluteWaarde(109);
            case GROEN: return getLocatieWithAbsoluteWaarde(126);
            default: throw new ParchisException("De volgende kleur wordt doorgegeven: " + kleur +
                    ". \n Ofwel bestaat deze niet ofwel moet deze functie nog aangepast worden.\n");
        }
    }

}