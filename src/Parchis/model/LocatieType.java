package Parchis.model;

public enum LocatieType {
    NEST,
    RONDGANG,
    LANDINGZONE,
    UITGESPEELD
}