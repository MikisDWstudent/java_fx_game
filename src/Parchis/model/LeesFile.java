package Parchis.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class LeesFile {
    //heel eenvoudige class om onze about & spelregels in te lezen

    private String content = "";

    public LeesFile(String LOCATIE_FILE) {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream("/" + LOCATIE_FILE)))){
            String line = "";
            while ((line=reader.readLine())!=null){
                content += line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getContent(){
        return content;
    }
}
