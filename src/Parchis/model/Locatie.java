package Parchis.model;

public class Locatie {
    private final int row;
    private final int column;
    private final int rowSpan;
    private final int colSpan;
    private final LocatieType locatieType;
    private final int absoluteWaarde;
    private final int[] safeSpaces = {5, 12, 17, 22, 29, 34, 39, 46, 51, 56, 63, 68};

    public Locatie(int row, int column, int rowSpan, int colSpan, int absoluteWaarde, LocatieType locatieType) {
        this.row = row;
        this.column = column;
        this.rowSpan = rowSpan;
        this.colSpan = colSpan;
        this.absoluteWaarde = absoluteWaarde;
        this.locatieType = locatieType;
    }

    @Override
    public int hashCode() {
        return 31 * absoluteWaarde;
    }

    @Override
    public boolean equals(Object object) {
        // Van zodra de absolute waarde gelijk is, een object eigenlijk hetzelfde
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;

        Locatie locatie = (Locatie) object;
        return absoluteWaarde == locatie.absoluteWaarde;
    }


    public int getRelatievewaarde(Kleur kleur){
        //Als geel 5 gooit, wordt hij vrijgelaten op positie 5. Als blauw 5 gooit, wordt hij vrijgelaten op 22.
        //Dat wil zeggen dat 22 voor blauw eigenlijk 5 is voor geel (dus subtract = 17)
        int subtract;
        switch (kleur) {
            case GEEL: subtract = 0; break;
            case BLAUW: subtract = 17; break;
            case ROOD: subtract = 34; break;
            case GROEN: subtract = 51; break;
            default: throw new ParchisException("De volgende kleur wordt doorgegeven: " + kleur +
                    ". \n Ofwel bestaat deze niet ofwel moet deze functie nog aangepast worden.\n");
        }
        if (this.locatieType == LocatieType.NEST) {
            return 0; //elk nest is los van zijn kleur 0
        } else if (this.locatieType == LocatieType.LANDINGZONE) {
            return this.absoluteWaarde - subtract;
        } else if (subtract >= absoluteWaarde) {
            /*
            Bijvoorbeeld positie 5 kan -12 zijn voor blauw, maar dat wil
            wellicht zeggen dat hij op plaats 67 staat & 6 gegooid heeft.
             */
            return 68 - (subtract - absoluteWaarde);
        } else {
            return this.absoluteWaarde - subtract;
        }
    }

    public int stappenVerschil(Locatie andereLocatie) {
        if (this.absoluteWaarde <= andereLocatie.absoluteWaarde) {
            return 68 - andereLocatie.absoluteWaarde + this.absoluteWaarde;
        } else {
            return this.absoluteWaarde - andereLocatie.absoluteWaarde;
        }
    }

    public boolean isSafeSpace() {
        //check if the absolute value is in our list of safe spaces
        for (int element : this.safeSpaces) {
            if (element == this.absoluteWaarde) {
                return true;
            }
        }
        return false;
    }

    public int getAbsoluteWaarde() {
        return absoluteWaarde;
    }
    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public int getRowSpan() {
        return rowSpan;
    }

    public int getColSpan() {
        return colSpan;
    }

    public LocatieType getLocatieType() {
        return locatieType;
    }

    @Override
    public String toString() {
        return String.format("Dit is locatie met absolute waarde: %d", absoluteWaarde) + "\n" +
                String.format("Dit is een locatie van het type %s", locatieType) + "\n" +
                String.format("bevindt zich op het bord op rij %d en kolom %d", row, column) + "\n"
        ;
    }
}