package Parchis.model;

public class Pion {
    private Kleur kleur;
    private int index;
    private Locatie locatie;

    private final LocatieGenerator locatieGenerator = new LocatieGenerator();

    public Pion(Kleur kleur, int index) {
        this.kleur = kleur;
        this.index = index;
        setLocatie(getNestLocatie());
    }

    public Locatie getLocatie() {
        return locatie;
    }

    public void setLocatie(Locatie locatie) {
        this.locatie = locatie;
    }

    public Kleur getKleur() {
        return kleur;
    }

    public int getIndex() {
        return index;
    }

    public Locatie getStartLocatie() {
        return locatieGenerator.getStartLocatie(this.kleur);
    }

    public Locatie getStartRondgangLocatie() {
        return locatieGenerator.getStartLandingZoneLocatie(this.kleur);
    }

    public Locatie getNestLocatie() {
        Locatie blauwNest = locatieGenerator.getLocatieWithAbsoluteWaarde(-3);
        Locatie geelNest = locatieGenerator.getLocatieWithAbsoluteWaarde(-2);
        Locatie roodNest = locatieGenerator.getLocatieWithAbsoluteWaarde(-1);
        Locatie groenNest = locatieGenerator.getLocatieWithAbsoluteWaarde(0);

        switch (this.kleur) {
            case BLAUW: return blauwNest;
            case GEEL: return geelNest;
            case ROOD: return roodNest;
            case GROEN: return groenNest;
            default: throw new ParchisException("De volgende kleur wordt doorgegeven: " + kleur +
                    ". \n Ofwel bestaat deze niet ofwel moet deze functie nog aangepast worden.\n");
        }
    }
}
