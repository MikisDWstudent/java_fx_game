package Parchis.model;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class TxtReader {

    private final Path filePath;
    private String content = "";

    public TxtReader(Path filePath) {
        this.filePath = filePath;
        readFile();
    }

    public void readFile() {
        if (Files.exists(filePath)) {
            try {
                Scanner fileScanner = new Scanner(filePath);
                while (fileScanner.hasNext()) {
                    content += fileScanner.nextLine() + "\n";
                }
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }
    }

    public String getContent() {
        return content;
    }
}
