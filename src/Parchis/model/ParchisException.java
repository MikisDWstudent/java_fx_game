package Parchis.model;

public class ParchisException extends RuntimeException{
    public ParchisException(String s) { super(s); }

    public ParchisException(Throwable cause) { super(cause); }
}
