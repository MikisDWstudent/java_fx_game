package Parchis.model;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.*;
import java.util.*;

public class Highscore {
    static Path HighscoreFile = Paths.get("resources" + File.separator+ "bestanden" + File.separator + "highscore.txt");
    public static final String HIGHSCORE_FILE = "bestanden/highscore.txt";
    private boolean isSortedAscending;
    private ArrayList<HighScoreRecord> highScoreList = new ArrayList<>();

    public Highscore() {
        //lees de txt in & sort ascending
        readFile();
        sortHighscoreAscending(true);
    }

    public void readFile() {
        //maak hem leeg als hij al data bevatte
        highScoreList.clear();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                this.getClass().getResourceAsStream("/" + HIGHSCORE_FILE)))){
            String line = "";
            while ((line=reader.readLine())!=null){
                String[] parts = line.split(",");
                String name = parts[0];
                int aantalBeurten = Integer.parseInt(parts[1]);
                highScoreList.add(new HighScoreRecord(name, aantalBeurten));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeFile() {
        try {
            Files.deleteIfExists(HighscoreFile);
            Files.createFile(HighscoreFile);
            StringBuilder s = new StringBuilder();
            for (HighScoreRecord highScoreRecord : highScoreList) {
                s.append(highScoreRecord).append("\n");
            }
            //laat die laatste \n vallen
            s.setLength(s.length() - 1);
            Files.write(HighscoreFile, s.toString().getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sortHighscoreAscending(boolean sortAscending) {
        this.isSortedAscending = sortAscending;
        //Sorteer op basis van aantalBeurten (gedefinieerd in HighScoreRecord class)
        if (isSortedAscending) {
            Collections.sort(highScoreList);
        } else {
            Collections.sort(highScoreList, Collections.reverseOrder());
        }
    }

    public void swapSortOrder() {
        //draai de volgorde om.
        sortHighscoreAscending(!isSortedAscending);
    }

    public void addHighscoreRecord(String naam, int aantalBeurten) {
        highScoreList.add(new HighScoreRecord(naam, aantalBeurten));
    }

    public ArrayList<HighScoreRecord> getHighScoreList() {
        return highScoreList;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < highScoreList.size(); i++) {
            String naam = highScoreList.get(i).getNaam();
            int aantalBeurten = highScoreList.get(i).getAantalBeurten();
            String beurtStr = aantalBeurten==1 ? "beurt" : "beurten";

            str.append(String.format("%d) %s - %d %s \n"
                    , i+1
                    , naam
                    , aantalBeurten
                    , beurtStr));
        }
        return str.toString();
    }
}