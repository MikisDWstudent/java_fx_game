package Parchis.model;

import java.util.ArrayList;

public class ParchisModel {
    /*
    The model is an interface defining the data to be displayed
    or otherwise acted upon in the user interface.
     */
    public static final int AANTAL_SPELERS = 4;
    public static final int PIONNEN_PER_SPELER = 4;
    private final Highscore highscore = new Highscore();
    private int aantalOgen;
    private final Dobbelsteen dobbelsteen;
    private final LocatieGenerator locatieGenerator = new LocatieGenerator();
    private final PionGenerator pionGenerator = new PionGenerator();
    private Pion laatstVerzettePion;

    public ParchisModel() {
        this.aantalOgen = 1;
        dobbelsteen = new Dobbelsteen();
    }

    public void werp(){
        aantalOgen =  dobbelsteen.werp();
    }

    public int getAantalOgen() {
        return aantalOgen;
    }

    public Highscore getHighscore() {
        return highscore;
    }

    public boolean isZetMogelijk(Locatie huidigeLocatie, int pionIndex, Kleur kleur, int worp) {
        //kijkt of een pion vanuit een bepaalde positie iets kan doen met een worp
        if (blockageDoorNest(kleur, worp, pionIndex)) {
            return false;
        }
        if (worp == 6 && heeftSpelerBarriere(kleur) && !vormtPionBarriere(kleur, pionIndex)) {
            return false;
        }
        //Als een zet niet mogelijk is, genereert getVolgendeLocatie een resultaat met waarde -999
        //Als de pion uitgespeeld is, staat hij op een dummy locatie met waarde 999
        Locatie suggestie = getValidatedVolgendeLocatie(kleur, pionIndex, worp);
        boolean zetOnmogelijk = suggestie.getAbsoluteWaarde() == -999 || huidigeLocatie.getAbsoluteWaarde() == 999;
        return !zetOnmogelijk;
    }

    private boolean blockageDoorNest(Kleur kleur, int worp, int pionIndex) {
        //deze methode test of de pion geblokkeerd wordt omdat er 5 gegooid is en er nog pionnen in het nest zitten
        return spelerMoetNestVerzetten(kleur, worp) && getPionLocatieType(kleur, pionIndex) != LocatieType.NEST;
    }

    private boolean spelerMoetNestVerzetten(Kleur kleur, int worp) {
        //deze methode test of er 5 gegooid is en de speler nog minstens 1 pion in het nest heeft
        //als dat zo is, geeft deze methode true terug en moet een pion verzet worden
        return worp == 5 && !allePionnenOpVeld(kleur);
    }

    public boolean allePionnenOpVeld(Kleur kleur) {
        for (int i = 0; i < PIONNEN_PER_SPELER; i++) {
            if (getPionLocatieType(kleur, i + 1) == LocatieType.NEST) {
                return false;
            }
        }
        return true;
    }

    private LocatieType getPionLocatieType(Kleur kleur, int pionIndex) {
        Pion pion = getPion(kleur, pionIndex);
        LocatieType locatieType = pion.getLocatie().getLocatieType();
        return locatieType;
    }

    public Kleur getVolgendeSpeler(Kleur huidigeSpeler) {
        switch (huidigeSpeler) {
            case BLAUW: return Kleur.GEEL;
            case GEEL: return Kleur.GROEN;
            case GROEN: return Kleur.ROOD;
            case ROOD: return Kleur.BLAUW;
            default: throw new ParchisException("De volgende kleur wordt doorgegeven: " + huidigeSpeler +
                    ". \n Ofwel bestaat deze niet ofwel moet deze functie nog aangepast worden.\n");
        }
    }
    public Locatie getVolgendeLocatie(Kleur kleur, int pionIndex, int worp) {
        //dit geeft de locatie weer waar je op belandt vertrekkende van deze locatie en gegeven een
        //bepaalde worp
        //als een speler alle pionnen op het veld heeft en hij gooit 6, mag hij 7 stappen zetten
        if (allePionnenOpVeld(kleur) && worp == 6) {
            worp = 7;
        }
        Pion pion = pionGenerator.getPionKleurIndex(kleur, pionIndex);
        Locatie huidigeLocatie = pion.getLocatie();

        if (huidigeLocatie.getLocatieType() == LocatieType.UITGESPEELD) {
            //Dit is geen error, dit kan perfect mogelijk zijn
            return new Locatie(0, 0, 0, 0, -999, LocatieType.NEST);
        } else if (huidigeLocatie.getLocatieType() == LocatieType.NEST && worp != 5) {
            return new Locatie(0, 0, 0, 0, -999, LocatieType.NEST);
        } else if (huidigeLocatie.getLocatieType() == LocatieType.NEST && worp == 5) {
            return locatieGenerator.getStartLocatie(kleur);
        } else if (huidigeLocatie.getLocatieType() == LocatieType.RONDGANG) {
            int huidigeRelWaarde = huidigeLocatie.getRelatievewaarde(kleur);
            //als je via een sprong van 20 (na het opeten van een pion) voorbij het einde van de landingzone komt,
            //dan neem je gewoon stapjes terug
            int maxRelWaarde = locatieGenerator.getLocatieMaxAbsoluteWaardeKleur(kleur).getRelatievewaarde(kleur);
            if (huidigeRelWaarde + worp == 76) { //dan speel je uit
                return new Locatie(0, 0, 0, 0, 999, LocatieType.UITGESPEELD);
            } else if (huidigeRelWaarde + worp <= maxRelWaarde) { //dan mag je gewoon verzetten
                return locatieGenerator.getLocatieWithRelatieveWaarde(huidigeRelWaarde + worp, kleur);
            } else {
                int stappenTotLandingzone = 69 - huidigeRelWaarde; //vanaf 69 zit je in de landingzone
                int stappenInLandingzone = 7; // 69 + 7 = 76 = uitgespeeld
                int blijftOver = worp - stappenTotLandingzone;
                int aantalKeerInZeven = blijftOver / stappenInLandingzone;
                int modulo = blijftOver % stappenInLandingzone;
                if (modulo == 0) {
                    //dan ben je uitgespeeld
                    return new Locatie(0, 0, 0, 0, 999, LocatieType.UITGESPEELD);
                } else if((aantalKeerInZeven%2)==0) {
                    //https://stackoverflow.com/questions/7342237/check-whether-number-is-even-or-odd
                    // dan ga je voorwaarts (begin landingzone naar einde)
                    return locatieGenerator.getLocatieWithRelatieveWaarde(69 + modulo, kleur);
                } else {
                    // dan ga je achterwaarts (van einde naar begin landingzone)
                    return locatieGenerator.getLocatieWithRelatieveWaarde(76 - modulo, kleur);
                }
            }
        } else { //zit je in de landingzone
            //hier hoeven we geen rekening te houden met een worp van 20, want dat is altijd vanuit een rondgang (door
            //een pion op eten die op de rondgang staat)
            //relatieve waarde + worp moet exact 76 zijn, dan ben je uit
            if (huidigeLocatie.getRelatievewaarde(kleur) + worp == 76) {
                return new Locatie(0, 0, 0, 0, 999, LocatieType.UITGESPEELD);
            } else if (huidigeLocatie.getRelatievewaarde(kleur) + worp < 76) {
                return locatieGenerator.getLocatieWithRelatieveWaarde(huidigeLocatie.getRelatievewaarde(kleur) + worp, kleur);
            } else {
                int idealeGooi = 76 - huidigeLocatie.getRelatievewaarde(kleur);
                int overschot = worp - idealeGooi;
                return locatieGenerator.getLocatieWithRelatieveWaarde(76 - overschot, kleur);
            }
        }
    }

    public boolean isSpelerGewonnen(Kleur kleur) {
        //het spel is gewonnen als één speler alle 4 zijn pionnen heeft uitgespeeld
        for (int i = 0; i < PIONNEN_PER_SPELER; i++) {
            int index = i + 1;
            Pion pion = getPion(kleur, index);
            LocatieType locatieType = pion.getLocatie().getLocatieType();
            if (locatieType != LocatieType.UITGESPEELD) {
                return false;
            }
        }
        return true;
    }

    public Locatie getValidatedVolgendeLocatie(Kleur kleur, int pionIndex, int worp) {
        //De doelLocatie is de "wiskundige" locatie, waar de pion op zou belanden als er geen constraints zijn
        //We returnen echter een "infeasible" locatie als er meer dan 1 pion van éénzelfde kleur daar staat
        Locatie doelLocatie =  getVolgendeLocatie(kleur, pionIndex, worp);
        Pion pion = pionGenerator.getPionKleurIndex(kleur, pionIndex);
        Locatie huidigeLocatie = pion.getLocatie();

        ArrayList<Locatie> tussenLiggendeLocaties = getTussenliggendeLocaties(huidigeLocatie, doelLocatie, kleur);
        ArrayList<Pion> pionnenOpLocatie = getPionnenOpLocatie(doelLocatie);

        if (isOnderwegBarriere(tussenLiggendeLocaties, kleur)) { //kijk of hij een barriere tegenkomt
            return new Locatie(0,0,0,0,-999, LocatieType.NEST);
        }
        if (pionnenOpLocatie.isEmpty()) { //om te voorkomen dat de code hieronder een error geeft bij get(0)
            return doelLocatie;
        }
        return doelLocatie;
    }

    public void setPionLocatie(Kleur kleur, int pionIndex, Locatie locatie) {
        Pion pion = pionGenerator.getPionKleurIndex(kleur, pionIndex);
        pion.setLocatie(locatie);
        //hou bij dat dit de laatste verzette pion is
        this.laatstVerzettePion = pion;
    }
    public boolean isLocatieBarriere(Locatie locatie, Kleur kleur) {
        ArrayList<Pion> pionnenOpLocatie = getPionnenOpLocatie(locatie);
        //als er geen pionnen op die locatie staan, is het automatisch geen barriere
        if (pionnenOpLocatie.isEmpty()) {
            return false;
        }
        //anders gaan we kijken of er meer dan 1 pion op die locatie staat EN of dit een safe space is
        //in de 2 rijen hieronder is het enkel een barriere als die door 2 pionnen van een andere kleur is gemaakt
        //volgens de opgave maakt de kleur niet uit, je kan ook jezelf blokkeren
        //Kleur bezetteKleur = pionnenOpLocatie.get(0).getKleur();
        //return bezetteKleur != kleur && pionnenOpLocatie.size() > 1;
        return locatie.isSafeSpace() && pionnenOpLocatie.size() > 1;
    }

    public boolean isOnderwegBarriere(ArrayList<Locatie> locaties, Kleur kleur) {
        boolean isBarriere = false;
        for (Locatie locaty : locaties) {
            boolean locatieIsBarriere = isLocatieBarriere(locaty, kleur);
            isBarriere = (isBarriere || locatieIsBarriere);
        }
        return isBarriere;
    }

    public boolean vormtPionBarriere(Kleur kleur, int pionIndex) {
        Pion pion = pionGenerator.getPionKleurIndex(kleur, pionIndex);
        Locatie huidigeLocatie = pion.getLocatie();
        ArrayList<Pion> pionnenOpLocatie = getPionnenOpLocatie(huidigeLocatie);
        return huidigeLocatie.isSafeSpace() && pionnenOpLocatie.size() > 1;
    }

    public boolean heeftSpelerBarriere(Kleur kleur) {
        boolean heeftBarriere = false;
        for (int i = 0; i < PIONNEN_PER_SPELER; i++) { //4 pionnen
            int index = i + 1;
            boolean vormtBarriere = vormtPionBarriere(kleur, index);
            heeftBarriere = (vormtBarriere || heeftBarriere);
        }
        return heeftBarriere;
    }

    public ArrayList<Locatie> getTussenliggendeLocaties(Locatie vertrekLocatie, Locatie doelLocatie, Kleur kleur) {
        ArrayList<Locatie> tussenliggendeLocaties = new ArrayList<Locatie>();
        int startRelatieveWaarde = vertrekLocatie.getRelatievewaarde(kleur);
        int eindRelatieveWaarde = doelLocatie.getRelatievewaarde(kleur);

        if(vertrekLocatie.getLocatieType() == LocatieType.NEST) {
            //Dan is de enige mogelijke tussenliggende Locatie de startlocatie van die kleur
            tussenliggendeLocaties.add(pionGenerator.getPionKleurIndex(kleur, 1).getStartLocatie());
            return tussenliggendeLocaties;
        }

        int verschilRelatieWaarde = eindRelatieveWaarde - startRelatieveWaarde;

        for (int i = 0; i < verschilRelatieWaarde; i++) {
            int relatieveWaarde = startRelatieveWaarde + i + 1;
            try {
                Locatie tussenliggendeLocatie = locatieGenerator.getLocatieWithRelatieveWaarde(relatieveWaarde, kleur);
                tussenliggendeLocaties.add(tussenliggendeLocatie);
            } catch (ParchisException p) {
                //als de tussenliggendeLocatie niet bestaat, dan geeft die een parchis exception terug
                //We kunnen dit gewoon negeren (dan voegen we die gewoon niet toe aan tussenliggendeLocaties
            }
        }
        return tussenliggendeLocaties;
    }

    public String getZetInfo(Kleur kleur, int pionIndex, int worp) {
        Locatie wiskundigeLocatie = getVolgendeLocatie(kleur, pionIndex, worp);
        Locatie doelLocatie = getValidatedVolgendeLocatie(kleur, pionIndex, worp);
        ArrayList<Pion> pionnenOpLocatie = getPionnenOpLocatie(doelLocatie);
        Pion pion = pionGenerator.getPionKleurIndex(kleur, pionIndex);
        Locatie huidigeLocatie = pion.getLocatie();

        if (aantalOgen == 0) { //dan is er nog niet gedobbeld
            return "dobbel voor omschrijving";
        }
        //eerst willen we weten of er onderweg een barriere is
        //ArrayList<Locatie> tussenLiggendeLocaties = getTussenliggendeLocaties(huidigeLocatie, doelLocatie, kleur);
        //aangezien de berekening hierboven barrieres uitsluit, kijken we of we oorspronkelijk ook geen barriere tegenkomen
        ArrayList<Locatie> wiskundigTussenLiggendeLocaties = getTussenliggendeLocaties(huidigeLocatie, wiskundigeLocatie, kleur);
        if ((huidigeLocatie.getLocatieType() == LocatieType.UITGESPEELD)) {
            return "Pion is uitgespeeld";
        }
        if ((blockageDoorNest(kleur, worp, pionIndex))) {
            return "andere pion moet uit nest";
        }
        if ((huidigeLocatie.getLocatieType() == LocatieType.NEST && worp != 5)) {
            return "Pion zit vast in het nest";
        }
        if ((huidigeLocatie.getLocatieType() == LocatieType.NEST && worp == 5)) {
            return "Haal pion uit het nest";
        }
        if (worp == 6 && heeftSpelerBarriere(kleur) && vormtPionBarriere(kleur, pionIndex)) {
            return "Verbreek barriere";
        }
        if (worp == 6 && heeftSpelerBarriere(kleur) && !vormtPionBarriere(kleur, pionIndex)) {
            return "eerst barriere breken";
        }
        if (doelLocatie.getAbsoluteWaarde() == 999) {
            return "Pion uitspelen";
        }
        if (isOnderwegBarriere(wiskundigTussenLiggendeLocaties, kleur)) { //kijk of hij een barriere tegenkomt
            return "pion geblokkeerd door barriere";
        } else if (pionnenOpLocatie.size() == 0 ) {
            return "Zet pion naar lege locatie";
        } else if (pionnenOpLocatie.size() == 1 && kleur != pionnenOpLocatie.get(0).getKleur()) {
            return String.format("eet pion op van %s", pionnenOpLocatie.get(0).getKleur());
        } else return "verzet pion";
    }

    public String getFeedback(int worp, boolean allePionnenOpVeld) {
        if (worp == 6) {
            if (allePionnenOpVeld) {
                return "6 gooien + alle pionnen op het veld = 7 &  nog is gooien";
            } else {
                return "U hebt 6 gegooid, u mag nog eens";
            }
        } else if (worp == 5) {
            return "U hebt 5 gegooid, een pion mag uit het nest";
        } else if (worp == 10) {
            return "10 zetten gratis vanwege uitspelen pion";
        } else {
            return String.format("U hebt %d gegooid, geen speciale regel?", worp);
        }
    }

    public boolean zetEetPionOp(Kleur kleur, int pionIndex, int worp) {
        Locatie doelLocatie = getValidatedVolgendeLocatie(kleur, pionIndex, worp);
        ArrayList<Pion> pionnenOpLocatie = getPionnenOpLocatie(doelLocatie);
        //als er een pion op die locatie staat & het is van een andere kleur, is de zet een "opeetZet"
        return pionnenOpLocatie.size() == 1 && kleur != pionnenOpLocatie.get(0).getKleur();
    }

    public Pion getPion(Kleur kleur, int pionIndex) {
        return pionGenerator.getPionKleurIndex(kleur, pionIndex);
    }

    public Pion getLaatstVerzettePion() {
        return laatstVerzettePion;
    }

    public ArrayList<Pion> getPionnenOpLocatie(Locatie locatie) {
        //Deze functie geeft terug welke pionnen er staan op een gegeven locatie
        // 1 pion = deze pion wordt opgegeten
        // >1 pion = de locatie is onbereikbaar
        ArrayList<Pion> pionnenOpLocatie = new ArrayList<>();

        for (Kleur kleur : Kleur.values()) { //loopen over de kleur
            for (int i = 0; i < PIONNEN_PER_SPELER; i++) { //4 pionnen
                int index = i + 1;
                Pion pion = getPion(kleur, index);
                Locatie pionLocatie = pion.getLocatie();
                if (locatie.getAbsoluteWaarde() == pionLocatie.getAbsoluteWaarde()) {
                    pionnenOpLocatie.add(pion);
                }
            }
        }
        return pionnenOpLocatie;
    }

    public void setAantalOgen(int aantalOgen) {
        this.aantalOgen = aantalOgen;
    }
}
